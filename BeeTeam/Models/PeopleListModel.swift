//
//  PeopleListModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class PeopleListModel: Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var employees: [Employee]?
    var next: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        employees <- map["response.employees"]
        next <- map["response.next"]
    }
}
