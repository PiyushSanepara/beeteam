//
//  ProfileModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    
    var id: Int?
    var enFName: String?
    var arFName: String?
    var enMName: String?
    var arMName: String?
    var enLName: String?
    var arLName: String?
    var enPosition: String?
    var arPosition: String?
    var birthday: String?
    var arFullName: String?
    var enFullName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        id <- map["response.employee.id"]
        enFName <- map["response.employee.name.enFName"]
        arFName <- map["response.employee.name.arFName"]
        enMName <- map["response.employee.name.enMName"]
        arMName <- map["response.employee.name.arMName"]
        enLName <- map["response.employee.name.enLName"]
        arLName <- map["response.employee.name.arLName"]
        enPosition <- map["response.employee.position.enPosition"]
        arPosition <- map["response.employee.position.arPosition"]
        birthday <- map["response.employee.birthday"]
        
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
    }
}
