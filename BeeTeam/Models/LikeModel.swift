//
//  LikeModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 02/09/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class LikeModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var likes: [Employee]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        likes <- map["response.likes"]
    }
}

