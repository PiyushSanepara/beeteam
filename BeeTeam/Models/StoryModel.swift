//
//  StoryModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 25/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class StoryModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    
    var stories: [Story]?
    var next: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        
        stories <- map["response.stories"]
        next <- map["response.navigation.next"]
    }
}

class Story : Mappable {
    var storiesCount: Int?
    var id: Int?
    var enFName: String?
    var arFName: String?
    var enMName: String?
    var arMName: String?
    var enLName: String?
    var arLName: String?
    var arFullName: String?
    var enFullName: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        storiesCount <- map["storiesCount"]
        id <- map["employee.id"]
        enFName <- map["employee.name.enFName"]
        arFName <- map["employee.name.arFName"]
        enFName <- map["employee.name.enMName"]
        arFName <- map["employee.name.arMName"]
        enLName <- map["employee.name.enLName"]
        arLName <- map["employee.name.arLName"]
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
    }
}
