//
//  ErrorModel.swift
//  BeeTeam
//


import Foundation
import ObjectMapper

class ErrorModel : Mappable {
    var success: Bool?
    var errorCodes: [ErrorCodeModel]?
    var identifier: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorCodes <- map["errorcodes"]
        identifier <- map["identifier.authentication"]
    }
}

class ErrorCodeModel: Mappable {
    var code: Int64?
    var errordescription: String?
    var error: [Errors]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        errordescription <- map["errordescription"]
        error <- map["errors"]
    }
}

class Errors: Mappable {
    var Key : String?
    var Value: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Key <- map["Key"]
        Value <- map["Value"]
    }
}
