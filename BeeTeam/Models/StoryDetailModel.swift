//
//  StoryDetailModel.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 08/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class StoryDetailModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var stories: [StoryDetailData]?
    var mystories: [StoryDetailData]?
    var next: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        stories <- map["response.stories"]
        mystories <- map["response.mystories"]
        next <- map["response.navigation.next"]
    }
}

class StoryDetailData : Mappable {
    var id: Int?
    var textContent: String?
    var material: Materials?
    var backgroundColorHex: String?
    var backgroundImage: String?
    var mine: Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        textContent <- map["textContent"]
        material <- map["material"]
        backgroundColorHex <- map["backgroundColorHex"]
        backgroundImage <- map["backgroundImage"]
        mine <- map["mine"]
    }
}
