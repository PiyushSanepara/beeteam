//
//  LoginModel.swift
//  BeeTeam

import Foundation
import ObjectMapper

class LoginModel : Mappable, Codable {
    var success: Bool?
    var authentication: String?
    var errorMessage: String?
    var employee: Employee?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        employee <- map["response.employee"]
    }
}

class Employee : Mappable, Codable {
    var id: Int?
    var enFName: String?
    var arFName: String?
    var enMName: String?
    var arMName: String?
    var enLName: String?
    var arLName: String?
    var arFullName: String?
    var enFullName: String?
    var userPermissions: UserPermissions?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        enFName <- map["name.enFName"]
        arFName <- map["name.arFName"]
        enMName <- map["name.enMName"]
        arMName <- map["name.arMName"]
        enLName <- map["name.enLName"]
        arLName <- map["name.arLName"]
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
        
        id <- map["ID"]
        enFName <- map["Name.En_FName"]
        arFName <- map["Name.Ar_FName"]
        enMName <- map["Name.En_MName"]
        arMName <- map["Name.Ar_MName"]
        enLName <- map["Name.En_LName"]
        arLName <- map["Name.Ar_LName"]
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
        
        userPermissions <- map["userPermissions"]
    }
}

class UserPermissions : Mappable, Codable {
    var smPost: Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        smPost <- map["smPost"]
    }
}


