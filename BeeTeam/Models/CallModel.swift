//
//  CallModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class CallModel: Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var callList: [Call]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        callList <- map["response.calls"]
    }
}

class Call : Mappable {
    var id: Int?
    var callDateTime: String?
    var startTime: String?
    var endTime: String?
    var callStatus: Int?
    var callType: Int?
    var caller: Employee?
    var receiver: Employee?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        callDateTime <- map["callDateTime"]
        startTime <- map["startTime"]
        endTime <- map["endTime"]
        callStatus <- map["callStatus"]
        callType <- map["callType"]
        caller <- map["caller"]
        receiver <- map["receiver"]
    }
}

