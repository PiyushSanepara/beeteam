//
//  MessageModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageModel: Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var messageList: [Message]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        messageList <- map["response.messages"]
    }
}

class Message : Mappable {
    var id: Int?
    var sender: Employee?
//    var groupId: Int?
    var senderId: Int?
    var receiverId: Int?
    var messageText: String?
    var readDateTime: String?
    var receivedDateTime: String?
    var creationDateTime: String?
    var materials: [Materials]?
    var me = Bool()
    var isPlaying = Bool()
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["ID"]
        sender <- map["Sender"]
//        groupId <- map["Chat_Group_ID"]
        senderId <- map["Sender"]
        receiverId <- map["Receiver"]
        messageText <- map["MSG_Text"]
        readDateTime <- map["Read"]
        receivedDateTime <- map["Received"]
        creationDateTime <- map["Creation_Date"]
        materials <- map["Materials"]
        
        id <- map["id"]
        senderId <- map["senderId"]
        senderId <- map["Sender_ID"]
        receiverId <- map["receiverId"]
        receiverId <- map["Receiver_ID"]
        messageText <- map["mSGText"]
        readDateTime <- map["read"]
        receivedDateTime <- map["received"]
        creationDateTime <- map["creationDate"]
        materials <- map["materials"]
        
        if let sid = self.sender?.id, sid != 0 {
            me = (sid == UserDefault.getLoginDetails()?.employee?.id)
        } else if let sid = self.senderId, sid != 0 {
            me = (sid == UserDefault.getLoginDetails()?.employee?.id)
        } else {
            me <- map["me"]// because didnot get respond in Signal R
        }
    }
}

