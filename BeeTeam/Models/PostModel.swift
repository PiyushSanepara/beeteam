//
//  PostModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 16/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class PostModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    
    var posts: [Post]?
    var next: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        
        posts <- map["response.posts"]
        next <- map["response.navigation.next"]
    }
}

class Post : Mappable {
    var id: Int?
    var empId: Int?
    var enFName: String?
    var arFName: String?
    var enMName: String?
    var arMName: String?
    var enLName: String?
    var arLName: String?
    var arFullName: String?
    var enFullName: String?
    var picture: String?
    var publishDateTime: String?
    var textContent: String?
    var likesCount: Int?
    var topLikes = [Employee]()
    var commentsCount: Int?
    var shareCount: Int?
    var liked = Bool()
    var topComments = [Comment]()
    var materials: [Materials]?
    var basePost: Post?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        empId <- map["employee.id"]
        enFName <- map["employee.name.enFName"]
        arFName <- map["employee.name.arFName"]
        enMName <- map["employee.name.enMName"]
        arMName <- map["employee.name.arMName"]
        enLName <- map["employee.name.enLName"]
        arLName <- map["employee.name.arLName"]
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
        var picture_temp: String?
        picture_temp <- map["employee.picture"]
        picture = UrlConstant.baseUrlImage + (picture_temp ?? "")
        publishDateTime <- map["publishDateTime"]
        textContent <- map["textContent"]
        likesCount <- map["likesCount"]
        topLikes <- map["topLikes"]
        commentsCount <- map["commentsCount"]
        shareCount <- map["shareCount"]
        liked <- map["liked"]
        topComments <- map["topComments"]
        materials <- map["materials"]
        basePost <- map["basePost"]
    }
}

class Materials : Mappable {
    var id: Int?
    var fileName: String?
    var type: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        fileName <- map["fileName"]
        type <- map["type"]
        
        id <- map["ID"]
        fileName <- map["File_Name"]
        type <- map["Type"]
    }
}

enum MaterialType: String {
    case audio = "Audio"
    case Video = "Video"
    case Image = "Image"
}
