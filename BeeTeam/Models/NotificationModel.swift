//
//  NotificationModel.swift
//  BeeTeam
//
//  Created by CSL Piyush on 02/09/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var notifications: [NotificationCustom]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        notifications <- map["response.notifications"]
    }
}

class NotificationCustom: Mappable {
    var id: Int?
    var actionMaker: Employee?
    var isRead: Bool?
    var creationTime: String?
    var notificationText: String?
    var actionId: Int?
    var notificationType: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        actionMaker <- map["actionMaker"]
        isRead <- map["isRead"]
        creationTime <- map["creationTime"]
        notificationText <- map["notificationText"]
        actionId <- map["actionId"]
        notificationType <- map["notificationType"]
        
    }
}
