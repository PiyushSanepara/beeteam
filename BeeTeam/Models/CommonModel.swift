//
//  CommonModel.swift
//  BeeTeam
//
//  Created by CSL Piyush on 07/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class CommonModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    
    var unreadCount: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        
        unreadCount <- map["response.unreadCount"]
    }
}
