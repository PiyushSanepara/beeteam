//
//  CommentModel.swift
//  BeeTeam
//
//  Created by CSL Piyush on 07/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class CommentModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var comment: Comment?
    var comments: [Comment]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        comment <- map["response.comment"]
        comments <- map["response.comments"]

    }
}

class Comment: Mappable {
    var id: Int?
    var postId: Int?
    var textContent: String?
    var commentDate: String?
    var enFName: String?
    var arFName: String?
    var enMName: String?
    var arMName: String?
    var enLName: String?
    var arLName: String?
    var arFullName: String?
    var enFullName: String?
    var empId: Int?
    var replies: [Comment]?
    var isHideReplyView = true
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        postId <- map["postId"]
        textContent <- map["textContent"]
        commentDate <- map["commentDate"]
        enFName <- map["employee.name.enFName"]
        arFName <- map["employee.name.arFName"]
        enMName <- map["employee.name.enMName"]
        arMName <- map["employee.name.arMName"]
        enLName <- map["employee.name.enLName"]
        arLName <- map["employee.name.arLName"]
        arFullName = "\(arFName ?? "") \(arMName ?? "") \(arLName ?? "")"
        enFullName = "\(enFName ?? "") \(enMName ?? "") \(enLName ?? "")"
        empId <- map["employee.id"]
        replies <- map["replies"]        
    }
}
