//
//  ChatListModel.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatListModel : Mappable {
    var success: Bool?
    var errorMessage: String?
    var authentication: String?
    var chatList: [ChatList]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        errorMessage <- map["errorcodes.0.errordescription"]
        authentication <- map["identifier.authentication"]
        chatList <- map["response.people"]
        chatList <- map["response.groups"]
    }
}

class ChatList : Mappable {
    var employee: Employee?
    var lastMessage: Message?
    var groupHeader: String?
    var groupId: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        employee <- map["Employee"]
        employee <- map["employee"]
        lastMessage <- map["LastMessage"]
        lastMessage <- map["lastMessage"]
        groupHeader <- map["groupHeader"]
        groupId <- map["id"]
    }
}
