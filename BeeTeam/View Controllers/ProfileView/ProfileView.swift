//
//  ProfileView.swift
//  BeeTeam
//

import Foundation
import ObjectMapper

protocol ProfileView: class  {
    func profileWithSussess(profileModel: ProfileModel)
    func profileWithError(error: String)
    
    func postWithSussess(postModel: PostModel)
    func postWithError(error: String)
    
    func createPostWithSussess(commonModel: CommonModel)
    func createPostWithError(error: String)
    
    func likeWithSussess(commonModel: CommonModel)
    func likeWithError(error: String)
    
    func createCommentWithSussess(commentModel: CommentModel)
    func createCommentWithError(error: String)
    func updateProfileImageWithSussess(commonModel: CommonModel)
    func updateProfileImageWithError(error: String)
}
