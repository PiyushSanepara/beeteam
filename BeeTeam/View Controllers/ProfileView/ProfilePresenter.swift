//
//  ProfilePresenter.swift
//  BeeTeam
//

import Foundation


class ProfilePresenter: NSObject {
    let provider: ProfileProvider
    weak private var profileView: ProfileView?
    
    init(provider: ProfileProvider) {
        self.provider = provider
    }
    
    func attachView(view: ProfileView?) {
        guard let view = view else { return }
        profileView = view
    }
    
    func getEmployeeDetail(empId: Int) {
        provider.getEmployeeDetail(empId: empId, successHandler: { (response) in
        //            SharedPreferences.setAuthToken(authToken: response.token ?? "")
                    self.profileView?.profileWithSussess(profileModel: response)
        }) { (error) in
            self.profileView?.profileWithError(error: error)
        }
    }
    
    func getPost(next: String?, person: Int?, department: Int?) {
        provider.getPost(next: next, person: person, department: department, successHandler: { (response) in
            self.profileView?.postWithSussess(postModel: response)
        }) { (error) in
            self.profileView?.postWithError(error: error)
        }
    }
    
    func createPost(materialID: String?, text_Content: String?, postId: Int?) {
        provider.createPost(materialID: materialID, text_Content: text_Content, postId: postId, successHandler: { (response) in
            self.profileView?.createPostWithSussess(commonModel: response)
        }) { (error) in
            self.profileView?.createPostWithError(error: error)
        }
    }
    
    func like(id: Int, isLike: Bool) {
        provider.like(id: id, isLike: isLike, successHandler: { (response) in
            self.profileView?.likeWithSussess(commonModel: response)
        }) { (error) in
            self.profileView?.likeWithError(error: error)
        }
    }
    
    func createComment(id: Int, In_reply_to: Int?, CommentString: String?) {
        provider.createComment(id: id, In_reply_to: In_reply_to, CommentString: CommentString, successHandler: { (response) in
            self.profileView?.createCommentWithSussess(commentModel: response)
        }) { (error) in
            self.profileView?.createCommentWithError(error: error)
        }
    }
    
    func updateProfileImage(materialID: String?) {
        provider.updateImageWithID(materialID: materialID, successHandler: { (response) in
            self.profileView?.updateProfileImageWithSussess(commonModel: response)
        }) { (error) in
            self.profileView?.updateProfileImageWithError(error: error)
        }
    }
    
}
