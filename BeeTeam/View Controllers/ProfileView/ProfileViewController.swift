//
//  ProfileViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import AVFoundation
import YPImagePicker
import AVKit

class ProfileViewController: UIViewController {

    @IBOutlet var navigationTitleView: UIButton!
    @IBOutlet var profileTableview: UITableView!
        
    let profilePresenter = ProfilePresenter(provider: ProfileProvider())
    var profileData: ProfileModel?
    
    var postDataSource = [Post]()
    var nextData: String?
    var isFetchingData = false
    
    var employeeID: Int?
    
    private let refreshControl = UIRefreshControl()
    
    var selectedItems = [YPMediaItem]()
    let selectedImageV = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configNavigationBar()
        profileTableview.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
//        profileTableview.register(UINib(nibName: "SharedPostTableViewCell", bundle: nil), forCellReuseIdentifier: "SharedPostTableViewCell")
        
        profilePresenter.attachView(view: self)
        
        profilePresenter.getEmployeeDetail(empId: self.employeeID ?? (UserDefault.getLoginDetails()?.employee?.id ?? 0))
        isFetchingData = true
        profilePresenter.getPost(next: self.nextData, person: self.employeeID ?? (UserDefault.getLoginDetails()?.employee?.id), department: nil)
        
        self.profileTableview.estimatedRowHeight = 828
//        self.profileTableview.estimatedSectionHeaderHeight = 360
        
        self.configRefreshControll()
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
        self.setupView()
    }
    
    func configRefreshControll() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.profileTableview.refreshControl = refreshControl
        } else {
            self.profileTableview.addSubview(refreshControl)
        }
        refreshControl.tintColor = ConstantColor.Color_Orange
        
        refreshControl.addTarget(self, action: #selector(self.refreshProfileData), for: .valueChanged)
    }
    
    @IBAction func HomeAction(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
         let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
         let navigationController: NavigationController = NavigationController(rootViewController: homeViewController!)
         sideMenuViewController?.contentViewController = navigationController
    }
    
    @objc private func refreshProfileData() {
        self.postDataSource.removeAll()
        self.profileTableview.reloadData()
        self.nextData = nil
        
        isFetchingData = true
        profilePresenter.getPost(next: self.nextData, person: self.employeeID ?? (UserDefault.getLoginDetails()?.employee?.id), department: nil)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource, PostTableViewCellDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? 1 : self.postDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as? ProfileHeaderTableViewCell
            cell?.delegate = self
            cell?.updateProfileData(profileData: self.profileData)
            
            return cell ?? UITableViewCell()
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as? PostTableViewCell
            cell?.delegate = self
            
            cell?.commentTextfield.delegate = self
            cell?.commentTextfield.tag = indexPath.row
            cell?.likeButton.tag = indexPath.row
            cell?.commentButton.tag = indexPath.row
            cell?.shareButton.tag = indexPath.row
            cell?.btnLikeCount.tag = indexPath.row
            cell?.profileImageView.tag = indexPath.row
            
            let post = self.postDataSource[indexPath.row]
            cell?.likeButton.accessibilityLabel = String(post.likesCount ?? 0)
            
            cell?.updatePostData(post: post)
                    
            //        //Mark:- Pagination
            if indexPath.row == self.postDataSource.count - 2 { // last cell
                if !isFetchingData, self.nextData != nil { // more items to fetch
                    isFetchingData = true
                    profilePresenter.getPost(next: self.nextData, person: self.employeeID ?? (UserDefault.getLoginDetails()?.employee?.id), department: nil) // increment `fromIndex` by 20 before server call
                }
            }
            
            return cell ?? UITableViewCell()
        }
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return self.postDataSource.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileHeaderTableViewCell") as! ProfileHeaderTableViewCell
            cell.delegate = self
            cell.updateProfileData(profileData: self.profileData)
            
//            https://stackoverflow.com/questions/34417771/textfield-resignfirstresponder-disappears-uitableview-header
//            let view = UIView.init(frame: (cell.frame))
//            view.addSubview(cell)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as? PostTableViewCell
            cell?.delegate = self
            
            cell?.commentTextfield.delegate = self
            cell?.commentTextfield.tag = section - 1
            cell?.likeButton.tag = section - 1
            cell?.commentButton.tag = section - 1
            cell?.shareButton.tag = section - 1
            cell?.btnLikeCount.tag = section - 1
            cell?.profileImageView.tag = section - 1
            
            let post = self.postDataSource[section - 1]
            cell?.likeButton.accessibilityLabel = String(post.likesCount ?? 0)
            
            cell?.updatePostData(post: post)
                    
            //        //Mark:- Pagination
            if section - 1 == self.postDataSource.count - 2 { // last cell
                if !isFetchingData, self.nextData != nil { // more items to fetch
                    isFetchingData = true
                    profilePresenter.getPost(next: self.nextData, person: self.employeeID ?? (UserDefault.getLoginDetails()?.employee?.id), department: nil) // increment `fromIndex` by 20 before server call
                }
            }
            
            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else {
            return (self.postDataSource[section - 1].basePost == nil) ? 0 : 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SharedPostTableViewCell") as? PostTableViewCell
        
        cell?.delegate = self
        cell?.profileImageView.tag = indexPath.section - 1
        if let post = self.postDataSource[indexPath.section - 1].basePost {
            cell?.updateSharedPostData(post: post)
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }*/
    
    func likeAction(index: Int, isLiked: Bool, likeCount: Int) {
        let post = self.postDataSource[index]
        guard let postID = post.id else {
            return
        }
        
        profilePresenter.like(id: postID, isLike: !post.liked)
        self.postDataSource[index].liked = isLiked
        self.postDataSource[index].likesCount = likeCount
    }
    
    func profileImageTapped(index: Int) {
        //Beacuse you are already in profile
    }
    
    func showLikeBtnTapped(index: Int) {
        let post = self.postDataSource[index]
        guard let count = post.likesCount, count > 0 else { return }
        
        let stry = UIStoryboard(name: "Home", bundle: nil)
        guard let likeUserVC = stry.instantiateViewController(withIdentifier: "LikeUserViewController") as? LikeUserViewController else { return }
        likeUserVC.postId = post.id
        self.navigationController?.pushViewController(likeUserVC, animated: true)
    }
    
    func showCommentBtnTapped(index: Int) {
        let post = self.postDataSource[index]
        guard let count = post.commentsCount, count > 0 else { return }
        let stry = UIStoryboard(name: "Home", bundle: nil)
        if !(self.navigationController?.topViewController?.isKind(of: CommentUserViewController.self) ?? false) {
            guard let commentUserVC = stry.instantiateViewController(withIdentifier: "CommentUserViewController") as? CommentUserViewController else { return }
            commentUserVC.postId = post.id
            self.navigationController?.pushViewController(commentUserVC, animated: true)
        }
    }
    
    func shareAction(index: Int) {
        let post = self.postDataSource[index]
        
        let ac = UIAlertController(title: "Share Post", message: post.textContent, preferredStyle: .alert)
        ac.addTextField { tf in
            tf.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }

        let submitAction = UIAlertAction(title: "Add Photos or Videos", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            self.showPicker(textContent: answer?.text, postID: post.id)
        }
        
        let onlyTextAction = UIAlertAction(title: "Share Now", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            self.profilePresenter.createPost(materialID: nil, text_Content: answer?.text, postId: post.id)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        ac.addAction(submitAction)
        ac.addAction(onlyTextAction)
        ac.addAction(cancelAction)
        
        ac.actions[0].isEnabled = false
        ac.actions[1].isEnabled = false
        present(ac, animated: true)
    }
    
    @objc func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        
        alert.actions[0].isEnabled = (tf.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "")
        alert.actions[1].isEnabled = (tf.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "")
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
        let post = self.postDataSource[textField.tag]
        profilePresenter.createComment(id: post.id ?? 0, In_reply_to: nil, CommentString: textField.text)
        textField.text = nil
        return true
    }
}

extension ProfileViewController: ProfileView {
    
    func profileWithSussess(profileModel: ProfileModel) {
        
        self.profileData = profileModel
        profileTableview.reloadData()
    }
    
    func profileWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func postWithSussess(postModel: PostModel) {
        if (self.postDataSource.count == 0) {
            self.postDataSource = postModel.posts ?? []
        } else {
            self.postDataSource.append(contentsOf: postModel.posts ?? [])
        }
        self.nextData = postModel.next
        
        isFetchingData = false
        self.refreshControl.endRefreshing()
        self.profileTableview.reloadData()
    }
    
    func postWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func createPostWithSussess(commonModel: CommonModel) {
        print("Post created successfull")
        self.refreshProfileData()
    }
    
    func createPostWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func updateProfileImageWithSussess(commonModel: CommonModel) {
        self.refreshProfileData()
    }
    
    func updateProfileImageWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func likeWithSussess(commonModel: CommonModel) {
        
    }
    
    func likeWithError(error: String) {
        
    }
    
    func createCommentWithSussess(commentModel: CommentModel) {
        if let comment = commentModel.comment {
            
            let index = self.postDataSource.firstIndex { (post) -> Bool in
                return post.id == comment.postId
            }
            
            if let i = index {
                self.postDataSource[i].topComments.insert(comment, at: 0)
                
                self.postDataSource[i].commentsCount = (self.postDataSource[i].commentsCount ?? 0) + 1
//                self.profileTableview.reloadData()//Need to change
                self.profileTableview.reloadRows(at: [IndexPath(row: i, section: 1)], with: .none)// reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
            }
        }
    }
    
    func createCommentWithError(error: String) {
        
    }
}

extension ProfileViewController {
    func setupView() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "sidemenu"), style: .plain, target: self, action: #selector(sideMenuClicked))
//        menuButton.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = menuButton
    }
    
    @objc func sideMenuClicked() {
        presentRightMenuViewController()
    }
}

extension ProfileViewController: ProfileHeaderTableViewCellDelegate {
    func textDidEndEditing(cell: ProfileHeaderTableViewCell, textContent: String?) {
//        print("TextContent is: \(textContent)")
        
//        guard textContent?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 else {
//            self.view.endEditing(true)
//            return
//        }
        
        let ac = UIAlertController(title: "Create Post", message: textContent, preferredStyle: .alert)

        let submitAction = UIAlertAction(title: "Add Photos or Videos", style: .default) { (_) in
            self.showPicker(textContent: textContent, postID: nil)
        }
        
        let onlyTextAction = UIAlertAction(title: "Only Text", style: .default) { (_) in
            self.profilePresenter.createPost(materialID: nil, text_Content: textContent, postId: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        ac.addAction(submitAction)
        ac.addAction(onlyTextAction)
        ac.addAction(cancelAction)
        
        present(ac, animated: true)        
    }
    
    func showPicker(textContent: String? = nil, isUpdateProfileImage: Bool = false, postID: Int?) {
        
        var config = YPImagePickerConfiguration()
        config.library.mediaType = isUpdateProfileImage ? .photo : .photoAndVideo

        config.shouldSaveNewPicturesToAlbum = false

        config.video.compression = AVAssetExportPresetLowQuality
        
        config.startOnScreen = .library

        config.screens = isUpdateProfileImage ? [.library, .photo] : [.library, .photo, .video]
        
        config.video.libraryTimeLimit = 500.0

        config.showsCrop = .none

        config.wordings.libraryTitle = "Gallery"

        config.hidesStatusBar = false

        config.hidesBottomBar = false
        
        config.maxCameraZoomFactor = 2.0

        config.library.maxNumberOfItems = isUpdateProfileImage ? 1 : 5
        config.gallery.hidesRemoveButton = false
        
        config.library.preselectedItems = nil //selectedItems
        
        let picker = YPImagePicker(configuration: config)
        

        /* Multiple media implementation */
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            
            _ = items.map { print("🧀 \($0)") }
            
            self.selectedItems = items
            if let firstItem = items.first {
                switch firstItem {
                case .photo(let photo):
                    self.selectedImageV.image = photo.image
                    
                    let jpgImageData = photo.image.jpegData(compressionQuality: 0.7)
                                                            
                    UtilCommonWebService.socialUpload(qqfile: jpgImageData, qqfilename: "\((photo.asset?.value(forKey: "filename") as? String)?.removeExtension() ?? "defalutImageName").jpeg", successHandler: { (postId) in
                        if isUpdateProfileImage {
                            self.profilePresenter.updateProfileImage(materialID: "\(postId ?? 0)")
                        } else if let id = postID {
                            self.profilePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: id)
                        } else {
                            self.profilePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: nil)
                        }
                        picker.dismiss(animated: true, completion: nil)
                    }) { (error) in
                        alertShowWithOK("BeeTeam", message: error, viewController: self)
                        picker.dismiss(animated: true, completion: nil)
                    }
                    
                case .video(let video):
                    do {
                    let videoData = try Data(contentsOf: video.url, options: .mappedIfSafe)
                        print(videoData)
                        
                        UtilCommonWebService.socialUpload(qqfile: videoData, qqfilename: video.asset?.value(forKey: "filename") as? String, successHandler: { (postId) in
                                            
    //                        print(postId)
                            if isUpdateProfileImage {
                                self.profilePresenter.updateProfileImage(materialID: "\(postId ?? 0)")
                            } else if let id = postID {
                                self.profilePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: id)
                            } else {
                                self.profilePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: nil)
                            }
                            
                            picker.dismiss(animated: true, completion: nil)
                        }) { (error) in
                            alertShowWithOK("BeeTeam", message: error, viewController: self)
                            picker.dismiss(animated: true, completion: nil)
                        }
                    } catch  {
                    }
//                    self.selectedImageV.image = video.thumbnail
//
//                    let assetURL = video.url
//                    let playerVC = AVPlayerViewController()
//                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                    playerVC.player = player
//
//                    picker.dismiss(animated: true, completion: { [weak self] in
//                        self?.present(playerVC, animated: true, completion: nil)
//                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
//                    })
                    self.selectedImageV.image = video.thumbnail
                    let assetURL = video.url
                    let playerVC = AVPlayerViewController()
                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
                    playerVC.player = player
                    picker.dismiss(animated: true, completion: { [weak self] in
                        self?.present(playerVC, animated: true, completion: nil)
                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
                    })
                }
            }
        }

        present(picker, animated: true, completion: nil)
    }
    
    func editProfileImageTapped(cell: ProfileHeaderTableViewCell) {
        self.showPicker(textContent: "", isUpdateProfileImage: true, postID: nil)
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}
