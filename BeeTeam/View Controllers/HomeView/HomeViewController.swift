//
//  HomeViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 10/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import YPImagePicker
import AVKit
//import Photos

class HomeViewController: UIViewController {
    
    @IBOutlet var navigationTitleView: UIView!
    @IBOutlet var homeTableview: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    
    let homePresenter = HomePresenter(provider: HomeProvider())
    var postDataSource = [Post]()
    var storyDataSource = [Story]()
    var nextData: String?
    var nextDataStory: String?
    var nextDataMyStory: String?
    var isFetchingData = false
    var storyIndexTapped: IndexPath? = nil
    private let refreshControl = UIRefreshControl()
    
    //For Image Picker
    var selectedItems = [YPMediaItem]()
    let selectedImageV = UIImageView()
    
    var notificationsButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
        homeTableview.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
//        homeTableview.register(UINib(nibName: "SharedPostTableViewCell", bundle: nil), forCellReuseIdentifier: "SharedPostTableViewCell")
        homePresenter.attachView(view: self)
        isFetchingData = true
        homePresenter.getPost(next: self.nextData, person: nil, department: nil)
        homePresenter.getStory(next: self.nextDataStory)
        self.collectionView.semanticContentAttribute = .forceRightToLeft
        self.homeTableview.estimatedRowHeight = 828
//        self.homeTableview.estimatedSectionHeaderHeight = 360
        self.configRefreshControll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.homePresenter.getUnreadCount()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
        
        NSLayoutConstraint.activate([
            notificationsButton.widthAnchor.constraint(equalToConstant: 34),
            notificationsButton.heightAnchor.constraint(equalToConstant: 44),
        ])
                
        notificationsButton.setImage(UIImage(named: "icon_navigation"), for: .normal)
        notificationsButton.addTarget(self, action: #selector(notificationBarAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: notificationsButton)
        
        self.setupView()
    }
    
    func badgeLabel(withCount count: Int) -> UILabel {
        let badgeCount = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        badgeCount.translatesAutoresizingMaskIntoConstraints = false
        badgeCount.tag = 1949
        badgeCount.layer.cornerRadius = badgeCount.bounds.size.height / 2
        badgeCount.textAlignment = .center
        badgeCount.layer.masksToBounds = true
        badgeCount.textColor = .white
        badgeCount.font = badgeCount.font.withSize(12)
        badgeCount.backgroundColor = .systemRed
        badgeCount.text = String(count)
        return badgeCount
    }
    
    func showBadge(withCount count: Int) {
        let badge = badgeLabel(withCount: count)
        notificationsButton.addSubview(badge)

        NSLayoutConstraint.activate([
            badge.leftAnchor.constraint(equalTo: notificationsButton.leftAnchor, constant: 14),
            badge.topAnchor.constraint(equalTo: notificationsButton.topAnchor, constant: 4),
            badge.widthAnchor.constraint(equalToConstant: 20),
            badge.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    func removeBadge() {
        if let badge = notificationsButton.viewWithTag(1949) {
            badge.removeFromSuperview()
        }
    }
    
    func configRefreshControll() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            self.homeTableview.refreshControl = refreshControl
        } else {
            self.homeTableview.addSubview(refreshControl)
        }
        refreshControl.tintColor = ConstantColor.Color_Orange
        refreshControl.addTarget(self, action: #selector(self.refreshHomeData), for: .valueChanged)
    }
    
    @objc private func refreshHomeData() {
        self.homePresenter.getUnreadCount()
        self.refreshPostData()
        self.refreshStoryData()
    }
    
    private func refreshPostData() {
        self.postDataSource.removeAll()
        self.homeTableview.reloadData()
        self.nextData = nil
        
        isFetchingData = true
        homePresenter.getPost(next: self.nextData, person: nil, department: nil)
    }
    
    private func refreshStoryData() {
        self.storyDataSource.removeAll()
        self.collectionView.reloadData()
        self.nextDataStory = nil
        self.nextDataMyStory = nil
        
        homePresenter.getStory(next: self.nextDataStory)
    }
    
    @objc private func notificationBarAction() {
        let stry = UIStoryboard(name: "Home", bundle: nil)
        guard let notificationVC = stry.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController else { return }
        
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource, PostTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as? PostTableViewCell
        cell?.delegate = self
//        print("Cell For: \(section)")
        cell?.commentTextfield.delegate = self
        cell?.commentTextfield.tag = indexPath.row
        cell?.likeButton.tag = indexPath.row
        cell?.commentButton.tag = indexPath.row
        cell?.shareButton.tag = indexPath.row
        cell?.btnLikeCount.tag = indexPath.row
        cell?.profileImageView.tag = indexPath.row

        let post = self.postDataSource[indexPath.row]
        cell?.likeButton.accessibilityLabel = String(post.likesCount ?? 0)
        
        cell?.updatePostData(post: post)
        
//        //Mark:- Pagination
        if indexPath.row == self.postDataSource.count - 2 { // last cell
            if !isFetchingData, self.nextData != nil { // more items to fetch
                isFetchingData = true
                homePresenter.getPost(next: self.nextData, person: nil, department: nil) // increment `fromIndex` by 20 before server call
            }
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return self.postDataSource.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as? PostTableViewCell
        cell?.delegate = self
        print("Cell For: \(section)")
        cell?.commentTextfield.delegate = self
        cell?.commentTextfield.tag = section
        cell?.likeButton.tag = section
        cell?.commentButton.tag = section
        cell?.shareButton.tag = section
        cell?.btnLikeCount.tag = section
        cell?.profileImageView.tag = section

        let post = self.postDataSource[section]
        cell?.likeButton.accessibilityLabel = String(post.likesCount ?? 0)
        
        cell?.updatePostData(post: post)
        
//        //Mark:- Pagination
        if section == self.postDataSource.count - 2 { // last cell
            if !isFetchingData, self.nextData != nil { // more items to fetch
                isFetchingData = true
                homePresenter.getPost(next: self.nextData, person: nil, department: nil) // increment `fromIndex` by 20 before server call
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.postDataSource[section].basePost == nil) ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SharedPostTableViewCell") as? PostTableViewCell
        
        cell?.delegate = self
        cell?.profileImageView.tag = indexPath.section
        if let post = self.postDataSource[indexPath.section].basePost {
            cell?.updateSharedPostData(post: post)
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }*/
    
    func likeAction(index: Int, isLiked: Bool, likeCount: Int) {
        let post = self.postDataSource[index]
        guard let postID = post.id else {
            return
        }
        
        homePresenter.like(id: postID, isLike: !post.liked)
        self.postDataSource[index].liked = isLiked
        self.postDataSource[index].likesCount = likeCount
    }
    
    func profileImageTapped(index: Int) {
//        guard let indexPath = self.homeTableview.indexPath(for: cell) else { return }
        let post = self.postDataSource[index]
        
        let stry = UIStoryboard(name: "Home", bundle: nil)
        guard let profileVC = stry.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
        profileVC.employeeID = post.empId
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func showLikeBtnTapped(index: Int) {
        let post = self.postDataSource[index]
        guard let count = post.likesCount, count > 0 else { return }
        
        let stry = UIStoryboard(name: "Home", bundle: nil)
        guard let likeUserVC = stry.instantiateViewController(withIdentifier: "LikeUserViewController") as? LikeUserViewController else { return }
        likeUserVC.postId = post.id
        self.navigationController?.pushViewController(likeUserVC, animated: true)
    }
    
    func showCommentBtnTapped(index: Int) {
        let post = self.postDataSource[index]
        guard let count = post.commentsCount, count > 0 else { return }
        let stry = UIStoryboard(name: "Home", bundle: nil)
        if !(self.navigationController?.topViewController?.isKind(of: CommentUserViewController.self) ?? false) {
            guard let commentUserVC = stry.instantiateViewController(withIdentifier: "CommentUserViewController") as? CommentUserViewController else { return }
            commentUserVC.postId = post.id
            self.navigationController?.pushViewController(commentUserVC, animated: true)
        }
    }
    
    func shareAction(index: Int) {
//        guard let indexPath = self.homeTableview.indexPath(for: cell) else { return }
        let post = self.postDataSource[index]
        
        let ac = UIAlertController(title: "Share Post", message: post.textContent, preferredStyle: .alert)
        
        ac.addTextField { tf in
            tf.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }

        let submitAction = UIAlertAction(title: "Add Photos or Videos", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            
            self.showPicker(textContent: answer?.text, isStory: false, postID: post.id)
        }
        
        let onlyTextAction = UIAlertAction(title: "Share Now", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            
            self.homePresenter.createPost(materialID: nil, text_Content: answer?.text, postId: post.id)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        ac.addAction(submitAction)
        ac.addAction(onlyTextAction)
        ac.addAction(cancelAction)
        
        ac.actions[0].isEnabled = false
        ac.actions[1].isEnabled = false
        present(ac, animated: true)
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, StoriesCollectionViewCellDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ((section == 0) ? 1 : self.storyDataSource.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoriesCollectionViewCell", for: indexPath) as? StoriesCollectionViewCell
        
        cell?.btnAddStory.isHidden = (indexPath.section != 0)
        cell?.imageAddStory.isHidden = (indexPath.section != 0)
        
        if indexPath.section == 0 {
            
            cell?.delegate = self
            
            cell?.updateBorder(color: "#5A5A5A")
            cell?.userNameLabel.text = UserDefault.getLoginDetails()?.employee?.enFName
            if let empID = UserDefault.getLoginDetails()?.employee?.id, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(empID)") {
                cell?.userImageLabel.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        } else {
            cell?.updateBorder(color: "#D7A129")
            cell?.userNameLabel.text = self.storyDataSource[indexPath.row].enFName
            if let id = self.storyDataSource[indexPath.row].id , let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
                cell?.userImageLabel.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
        
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 { // my story details
            homePresenter.getMyStoryData(next: nextDataMyStory)
        } else { // Others story details
            storyIndexTapped = indexPath
            homePresenter.getStoryDetailData(id: self.storyDataSource[indexPath.row].id ?? 0)
        }
    }
    
    func addStoryAction(cell: StoriesCollectionViewCell?) {
        let ac = UIAlertController(title: "Create Story", message: "Share what you are thinking here...", preferredStyle: .alert)

        ac.addTextField { tf in
            tf.addTarget(self, action: #selector(self.textChanged), for: .editingChanged)
        }
        
        let submitAction = UIAlertAction(title: "Add Photos or Videos", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            self.showPicker(textContent: answer?.text, isStory: true, postID: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let onlyTextAction = UIAlertAction(title: "Only Text", style: .default) { [unowned ac] _ in
            let answer = ac.textFields?[0]
            self.homePresenter.createStory(materialID: nil, text_Content: answer?.text)
        }

        ac.addAction(submitAction)
        ac.addAction(onlyTextAction)
        ac.addAction(cancelAction)
        
        ac.actions[0].isEnabled = false
        ac.actions[1].isEnabled = false
        present(ac, animated: true)
    }
    
    @objc func textChanged(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        
        alert.actions[0].isEnabled = (tf.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "")
        alert.actions[1].isEnabled = (tf.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "")
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
        let post = self.postDataSource[textField.tag]
        homePresenter.createComment(id: post.id ?? 0, In_reply_to: nil, CommentString: textField.text)
        textField.text = nil
        return true
    }
}

extension HomeViewController: HomeView {
    func postWithSussess(postModel: PostModel) {
        if (self.postDataSource.count == 0) {
            self.postDataSource = postModel.posts ?? []
        } else {
            self.postDataSource.append(contentsOf: postModel.posts ?? [])
        }
        self.nextData = postModel.next
        
        isFetchingData = false
        self.refreshControl.endRefreshing()
        self.homeTableview.reloadData()
    }
    
    func postWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func storyWithSussess(storyModel: StoryModel) {
        if (self.storyDataSource.count == 0) {
            self.storyDataSource = storyModel.stories ?? []
        } else {
            self.storyDataSource.append(contentsOf: storyModel.stories ?? [])
        }
        self.nextDataStory = storyModel.next
        
        //isFetchingData = false
        self.collectionView.reloadData()
    }
    
    func storyWithError(error: String) {
        
    }
    
    func likeWithSussess(commonModel: CommonModel) {
        
    }
    
    func likeWithError(error: String) {
        
    }
    
    func createCommentWithSussess(commentModel: CommentModel) {
        if let comment = commentModel.comment {
            
            let index = self.postDataSource.firstIndex { (post) -> Bool in
                return post.id == comment.postId
            }
            
            if let i = index {
                self.postDataSource[i].topComments.insert(comment, at: 0)
                
                self.postDataSource[i].commentsCount = (self.postDataSource[i].commentsCount ?? 0) + 1
//                self.homeTableview.reloadData()//Need to change
                self.homeTableview.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)// reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
            }
        }
    }
    
    func createCommentWithError(error: String) {

    }
    
    func getStoryDetailWithSussess(storyDetailModel: StoryDetailModel) {
        if let tappedIndex = storyIndexTapped {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContentView") as! ContentViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.pages = [storyDetailModel]
                vc.currentIndex = tappedIndex.row
                vc.employeeName = self.storyDataSource[tappedIndex.row].arFullName 
                vc.employeeID = self.storyDataSource[tappedIndex.row].id
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func getStoryDetailWithWithError(error: String) {
        
    }
    
    func getMyStoryWithSussess(storyDetailModel: StoryDetailModel) {
        self.nextDataMyStory = storyDetailModel.next
        
        if storyDetailModel.mystories?.count == 0 { // Create Story
            self.addStoryAction(cell: nil)
        } else { // View Story
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContentView") as! ContentViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.pages = [storyDetailModel]
                vc.currentIndex = 0
                vc.employeeName = UserDefault.getLoginDetails()?.employee?.enFName
                vc.employeeID = UserDefault.getLoginDetails()?.employee?.id
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func getMyStoryWithWithError(error: String) {
        
    }
    
    func createStoryWithSussess(commonModel: CommonModel) {
        print("Story Create successfull")
    }
    
    func createStoryWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func createPostWithSussess(commonModel: CommonModel) {
        print("Post created successfull")
        self.refreshPostData()
    }
    
    func createPostWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
    
    func unreadCountWithSussess(unreadCount: Int) {
        if unreadCount == 0 {
            self.removeBadge()
        } else {
            self.showBadge(withCount: unreadCount)
        }
    }
    
    func unreadCountWithError(error: String) {
        self.removeBadge()
    }
}

extension HomeViewController {
    func setupView() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "sidemenu"), style: .plain, target: self, action: #selector(sideMenuClicked))
//        menuButton.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = menuButton
    }
    
    @objc func sideMenuClicked() {
        presentRightMenuViewController()
    }
}

extension HomeViewController {
    func showPicker(textContent: String?, isStory: Bool, postID: Int?) {
        
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photoAndVideo

        config.shouldSaveNewPicturesToAlbum = false

        config.video.compression = AVAssetExportPresetLowQuality
        
        config.startOnScreen = .library

        config.screens = [.library, .photo, .video]
        
        config.video.libraryTimeLimit = 500.0

        config.showsCrop = .none

        config.wordings.libraryTitle = "Gallery"

        config.hidesStatusBar = false

        config.hidesBottomBar = false
        
        config.maxCameraZoomFactor = 2.0

        config.library.maxNumberOfItems = 1
        config.gallery.hidesRemoveButton = false
        
        config.library.preselectedItems = nil //selectedItems
        
        let picker = YPImagePicker(configuration: config)
        

        /* Multiple media implementation */
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            
            _ = items.map { print("🧀 \($0)") }
            
            self.selectedItems = items
            if let firstItem = items.first {
                switch firstItem {
                case .photo(let photo):
                    self.selectedImageV.image = photo.image
                    
                    let jpgImageData = photo.image.jpegData(compressionQuality: 0.7)
                                        
                    UtilCommonWebService.socialUpload(qqfile: jpgImageData, qqfilename: "\((photo.asset?.value(forKey: "filename") as? String)?.removeExtension() ?? "defalutImageName").jpeg", successHandler: { (postId) in
                        
//                        print(postId)
                        
                        if isStory {
                            self.homePresenter.createStory(materialID: "\(postId ?? 0)", text_Content: textContent)
                        } else {
                            if let id = postID {
                                self.homePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: id)
                            } else {
                                self.homePresenter.createPost(materialID: "\(postId ?? 0)", text_Content: textContent, postId: nil)
                            }
                        }
                        
                        picker.dismiss(animated: true, completion: nil)
                    }) { (error) in
                        alertShowWithOK(message: error, viewController: self)
                        picker.dismiss(animated: true, completion: nil)
                    }
                case .video(let video):
                    
                    do {
                        let videoData = try Data(contentsOf: video.url, options: .mappedIfSafe)
                            print(videoData)
                            
                        UtilCommonWebService.socialUpload(qqfile: videoData, qqfilename: video.asset?.value(forKey: "filename") as? String, successHandler: { (postId) in
                                                
        //                        print(postId)
                                if isStory {
                                    self.homePresenter.createStory(materialID: "\(postId ?? 0)", text_Content: textContent)
                                } else {
                                    if let id = postID {
                                        self.homePresenter.createPost(materialID: "\(postId ?? 0)", text_Content:textContent, postId: id)
                                    } else {
                                        self.homePresenter.createPost(materialID: "\(postId ?? 0)", text_Content:textContent, postId: nil)
                                    }
                                }
                                
                                picker.dismiss(animated: true, completion: nil)
                            }) { (error) in
                                alertShowWithOK("BeeTeam", message: error, viewController: self)
                                picker.dismiss(animated: true, completion: nil)
                            }
                        } catch  {
                        }

//                    self.selectedImageV.image = video.thumbnail
//                    
//                    let assetURL = video.url
//                    let playerVC = AVPlayerViewController()
//                    let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
//                    playerVC.player = player
//                
//                    picker.dismiss(animated: true, completion: { [weak self] in
//                        self?.present(playerVC, animated: true, completion: nil)
//                        print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
//                    })
                }
            }
        }

        present(picker, animated: true, completion: nil)
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}
