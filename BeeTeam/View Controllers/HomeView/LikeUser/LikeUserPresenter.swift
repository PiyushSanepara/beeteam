//
//  LikeUserPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class LikeUserPresenter: NSObject {
    let provider: LikeUserProvider
    weak private var likeUserView: LikeUserView?
    
    init(provider: LikeUserProvider) {
        self.provider = provider
    }
    
    func attachView(view: LikeUserView?) {
        guard let view = view else { return }
        likeUserView = view
    }
    
    func getLikes(next: String?, postId: Int?) {
        provider.getLikes(next: next, postId: postId, successHandler: { (response) in
            self.likeUserView?.likesWithSussess(likeModel: response)
        }) { (error) in
            self.likeUserView?.likesWithError(error: error)
        }
    }
    
}
