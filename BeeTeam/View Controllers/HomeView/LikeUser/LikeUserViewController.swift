//
//  LikeUserViewController.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 25/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class LikeUserViewController: UIViewController {
    @IBOutlet var likeTableView: UITableView!
    @IBOutlet var navigationTitleView: UIButton!
    var likeListDataSource = [Employee]()
    
    let likeUserPresenter = LikeUserPresenter(provider: LikeUserProvider())
    var nextData: String?
    var postId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
        self.likeTableView.semanticContentAttribute = .forceRightToLeft
        self.likeTableView.estimatedRowHeight = 100
        self.likeTableView.tableFooterView = UIView(frame: .zero)
        
        likeUserPresenter.attachView(view: self)
        likeUserPresenter.getLikes(next: self.nextData, postId: self.postId)
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
    }
    
    @IBAction func HomeAction(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
         let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
         let navigationController: NavigationController = NavigationController(rootViewController: homeViewController!)
         sideMenuViewController?.contentViewController = navigationController
    }
    
}

extension LikeUserViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.likeListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikeUserTableViewCell") as! LikeUserTableViewCell
        cell.updateLikeUserData(data: likeListDataSource[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let empId = likeListDataSource[indexPath.row].id {
            let stry = UIStoryboard(name: "Home", bundle: nil)
            guard let profileVC = stry.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else { return }
            profileVC.employeeID = empId
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
    }
}

extension LikeUserViewController: LikeUserView {
    func likesWithSussess(likeModel: LikeModel) {
        self.likeListDataSource = likeModel.likes ?? []
        
        self.likeTableView.reloadData()
    }
    
    func likesWithError(error: String) {
        self.likeListDataSource.removeAll()
        self.likeTableView.reloadData()
        
        alertShowWithOK(message: error, viewController: self)
    }
}
