//
//  PreViewController.swift
//  ARStories
//
//  Created by Antony Raphel on 05/10/17.
//

import UIKit
import AVFoundation
import AVKit
import CoreMedia

class PreViewController: UIViewController, SegmentedProgressBarDelegate {

    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var textContentBottomView: UIView!
    @IBOutlet weak var textContentWithImageAndVideo: UILabel!
    
    var pageIndex : Int = 0
    var items: [StoryDetailModel] = []
    var item: [StoryDetailData]? = []
    var SPB: SegmentedProgressBar!
    var player: AVPlayer!
    let loader = ImageLoader()
    
    var employeeName: String?
    var employeeID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userProfileImage.layer.cornerRadius = self.userProfileImage.frame.size.height / 2;
        if let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(employeeID ?? 0)") {
            userProfileImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
        }
        
        self.lblUserName.text = employeeName
        
        if items[pageIndex].stories != nil {
            item = items[pageIndex].stories
        } else {
            item = items[pageIndex].mystories
        }
        
        
        SPB = SegmentedProgressBar(numberOfSegments: item?.count ?? 0, duration: 5)
        if #available(iOS 11.0, *) {
            SPB.frame = CGRect(x: 18, y: UIApplication.shared.statusBarFrame.height + 5, width: view.frame.width - 35, height: 3)
        } else {
            // Fallback on earlier versions
            SPB.frame = CGRect(x: 18, y: 15, width: view.frame.width - 35, height: 3)
        }
        
        SPB.delegate = self
        SPB.topColor = UIColor.white
        SPB.bottomColor = UIColor.white.withAlphaComponent(0.25)
        SPB.padding = 2
        SPB.isPaused = true
        SPB.currentAnimationIndex = 0
        SPB.duration = getDuration(at: 0)
        view.addSubview(SPB)
        view.bringSubviewToFront(SPB)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        UIView.animate(withDuration: 0.8) {
            self.view.transform = .identity
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.SPB.startAnimation()
            self.playVideoOrLoadImage(index: 0)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.main.async {
            self.SPB.currentAnimationIndex = 0
            self.SPB.cancel()
            self.SPB.isPaused = true
            self.resetPlayer()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - SegmentedProgressBarDelegate
    //1
    func segmentedProgressBarChangedIndex(index: Int) {
        playVideoOrLoadImage(index: index)
    }
    
    //2
    func segmentedProgressBarFinished() {
        if pageIndex == (self.items.count - 1) {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            _ = ContentViewControllerVC.goNextPage(fowardTo: pageIndex + 1)
        }
    }
    
    @IBAction func previousStory(_ sender: Any) {
        SPB.rewind()
    }
    
    @IBAction func skipStory(_ sender: Any) {
        SPB.skip()
    }
    
    //MARK: - Play or show image
    func playVideoOrLoadImage(index: NSInteger) {
        
        if let material = item?[index].material {
            if material.type == MaterialType.Image.rawValue {
                self.SPB.duration = 5
                self.imagePreview.isHidden = false
                self.videoView.isHidden = true
                self.textContent.isHidden = true
                
                if let url = URL(string: UrlConstant.baseURLMaterialStory + "?id=\(item?[index].id ?? 0)") {
                    DispatchQueue.main.async {
                        self.imagePreview.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_logo_placeholder"))
                    }
                }
                //self.imagePreview.imageFromServerURL(material.fileName ?? "")
            } else {
                self.imagePreview.isHidden = true
                self.videoView.isHidden = false
                self.textContent.isHidden = true
                
                resetPlayer()
                guard let url = NSURL(string: material.fileName ?? "") as URL? else {return}
                self.player = AVPlayer(url: url)
                
                let videoLayer = AVPlayerLayer(player: self.player)
                videoLayer.frame = view.bounds
                videoLayer.videoGravity = .resizeAspect
                self.videoView.layer.addSublayer(videoLayer)
                
                let asset = AVAsset(url: url)
                let duration = asset.duration
                let durationTime = CMTimeGetSeconds(duration)
                
                self.SPB.duration = durationTime
                self.player.play()
            }
            
            if let tc = item?[index].textContent {
                self.textContentBottomView.isHidden = false
                self.textContentWithImageAndVideo.isHidden = false
                self.textContentWithImageAndVideo.text = tc
            } else {
                self.textContentBottomView.isHidden = true
                self.textContentWithImageAndVideo.isHidden = true
            }
        } else {
            self.imagePreview.isHidden = true
            self.videoView.isHidden = true
            self.textContent.isHidden = false
            self.textContentBottomView.isHidden = true
            self.textContentWithImageAndVideo.isHidden = true
            self.textContent.text = item?[index].textContent
        }
        // to set background color
        self.view.backgroundColor = UIColor.hexStringToUIColor(hex: item?[index].backgroundColorHex ?? "")
    }
    
    // MARK: Private func
    private func getDuration(at index: Int) -> TimeInterval {
        var retVal: TimeInterval = 5.0
        if let material = item?[index].material {
            if material.type == MaterialType.Image.rawValue {
                retVal = 5.0
            } else {
                guard let url = URL(string: material.fileName ?? "") else { return retVal }
                let asset = AVAsset(url: url)
                let duration = asset.duration
                retVal = CMTimeGetSeconds(duration)
            }
        }else {
            return retVal
        }
        return retVal
    }
    
    private func resetPlayer() {
        if player != nil {
            player.pause()
            player.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    //MARK: - Button actions
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        resetPlayer()
    }
}
