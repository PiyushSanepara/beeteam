//
//  ARModel.swift
//  ARStories
//
//  Created by ANTONY RAPHEL on 06/09/18.
//

import Foundation

struct StroyDisplayDetails {
    var name: String = ""
    var imageUrl: String = ""
    var material: Materials?
    
    init(data: StoryDetailData, name: String?, imageUrl: String?) {
        self.name = name ?? ""
        self.imageUrl = imageUrl ?? ""
        self.material = data.material
    }
}
