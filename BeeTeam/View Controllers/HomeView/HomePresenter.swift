//
//  HomePresenter.swift
//  BeeTeam
//

import Foundation
import Alamofire

class HomePresenter: NSObject {
    let provider: HomeProvider
    weak private var homeView: HomeView?
    
    init(provider: HomeProvider) {
        self.provider = provider
    }
    
    func attachView(view: HomeView?) {
        guard let view = view else { return }
        homeView = view
    }
    
    func getPost(next: String?, person: Int?, department: Int?) {
        provider.getPost(next: next, person: person, department: department, successHandler: { (response) in
            self.homeView?.postWithSussess(postModel: response)
        }) { (error) in
            self.homeView?.postWithError(error: error)
        }
    }
    
    func getStory(next: String?) {
        provider.getStory(next: next, successHandler: { (response) in
            self.homeView?.storyWithSussess(storyModel: response)
        }) { (error) in
            self.homeView?.storyWithError(error: error)
        }
    }
    
    func like(id: Int, isLike: Bool) {
        provider.like(id: id, isLike: isLike, successHandler: { (response) in
            self.homeView?.likeWithSussess(commonModel: response)
        }) { (error) in
            self.homeView?.likeWithError(error: error)
        }
    }
    
    func createComment(id: Int, In_reply_to: Int?, CommentString: String?) {
        provider.createComment(id: id, In_reply_to: In_reply_to, CommentString: CommentString, successHandler: { (response) in
            self.homeView?.createCommentWithSussess(commentModel: response)
        }) { (error) in
            self.homeView?.createCommentWithError(error: error)
        }
    }
    
    func getStoryDetailData(id: Int) {
        provider.getStoryDetailData(id: id, successHandler: { (response) in
            self.homeView?.getStoryDetailWithSussess(storyDetailModel: response)
        }) { (error) in
            self.homeView?.getStoryDetailWithWithError(error: error)
        }
    }
    
    func getMyStoryData(next: String?) {
        provider.getMyStoryData(next: next, successHandler: { (response) in
            self.homeView?.getMyStoryWithSussess(storyDetailModel: response)
        }) { (error) in
            self.homeView?.getMyStoryWithWithError(error: error)
        }
    }
        
    func createStory(materialID: String?, text_Content: String?) {
        provider.createStory(materialID: materialID, text_Content: text_Content, successHandler: { (response) in
            self.homeView?.createStoryWithSussess(commonModel: response)
        }) { (error) in
            self.homeView?.createStoryWithError(error: error)
        }
    }
    
    func createPost(materialID: String?, text_Content: String?, postId: Int?) {
        provider.createPost(materialID: materialID, text_Content: text_Content, postId: postId, successHandler: { (response) in
            self.homeView?.createPostWithSussess(commonModel: response)
        }) { (error) in
            self.homeView?.createPostWithError(error: error)
        }
    }
    
    func getUnreadCount() {
        provider.getUnreadCount(successHandler: { (unreadCount) in
            self.homeView?.unreadCountWithSussess(unreadCount: unreadCount)
        }) { (error) in
            self.homeView?.unreadCountWithError(error: error)
        }
    }
}
