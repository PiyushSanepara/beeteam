//
//  CommentUserProvider.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class CommentUserProvider {
    func getComments(next: String?, postId: Int?, successHandler : @escaping(_ objects: CommentModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.comments(next: next, id: postId))
            .onSuccess { (response: CommentModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
}
