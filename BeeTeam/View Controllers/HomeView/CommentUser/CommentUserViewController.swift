//
//  CommentUserViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CommentUserViewController: UIViewController {

    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet var navigationTitleView: UIButton!
    
    let commentUserPresenter = CommentUserPresenter(provider: CommentUserProvider())
    var nextData: String?
    var postId: Int?
    
    var commentListDataSource = [Comment]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
        self.commentTableView.sectionHeaderHeight = UITableView.automaticDimension
        self.commentTableView.estimatedSectionHeaderHeight = 165
        self.commentTableView.rowHeight = UITableView.automaticDimension
        self.commentTableView.estimatedRowHeight = 119
        commentUserPresenter.attachView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        commentUserPresenter.getComments(next: self.nextData, postId: self.postId)
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
    }
}

extension CommentUserViewController: UITableViewDelegate, UITableViewDataSource, CommentTableViewCellDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return commentListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell") as? CommentTableViewCell
        cell?.delegate = self
        cell?.replyButton.tag = section
        cell?.setContent(comment: self.commentListDataSource[section])
        return cell
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentListDataSource[section].replies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentReplyTableViewCell") as? CommentReplyTableViewCell
        cell?.setContent(comment: self.commentListDataSource[indexPath.section].replies?[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func replyButton(section: Int) {
        self.commentListDataSource[section].isHideReplyView = false
        self.commentTableView.reloadData()
    }
    
    func cancelButtonAction(cell: CommentTableViewCell) {
        guard let indexPath = self.commentTableView.indexPath(for: cell) else { return }
        //        let comment = self.commentListDataSource[indexPath.section]
        self.commentListDataSource[indexPath.section].isHideReplyView = true
        cell.replyCommentView.isHidden = self.commentListDataSource[indexPath.section].isHideReplyView
    }
    
    func replyButtonAction(cell: CommentTableViewCell) {
        guard let indexPath = self.commentTableView.indexPath(for: cell) else { return }
//        let comment = self.commentListDataSource[indexPath.section]
        
        self.commentListDataSource[indexPath.section].isHideReplyView = true
        cell.replyCommentView.isHidden = self.commentListDataSource[indexPath.section].isHideReplyView
    }
}

extension CommentUserViewController: CommentUserView {
    func commentsWithSussess(commentModel: CommentModel) {
        self.commentListDataSource = commentModel.comments ?? []
        reloadTableView()
    }
    
    func commentsWithError(error: String) {
        self.commentListDataSource.removeAll()
        reloadTableView()
        
        alertShowWithOK(message: error, viewController: self)
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.commentTableView.reloadData()
        }
    }
}
