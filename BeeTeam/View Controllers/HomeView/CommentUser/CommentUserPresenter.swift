//
//  CommentUserPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class CommentUserPresenter: NSObject {
    let provider: CommentUserProvider
    weak private var commentUserView: CommentUserView?
    
    init(provider: CommentUserProvider) {
        self.provider = provider
    }
    
    func attachView(view: CommentUserView?) {
        guard let view = view else { return }
        commentUserView = view
    }
    
    func getComments(next: String?, postId: Int?) {
        provider.getComments(next: next, postId: postId, successHandler: { (response) in
            self.commentUserView?.commentsWithSussess(commentModel: response)
        }) { (error) in
            self.commentUserView?.commentsWithError(error: error)
        }
    }
    
}
