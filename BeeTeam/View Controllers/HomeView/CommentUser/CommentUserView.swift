//
//  CommentUserView.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import ObjectMapper

protocol CommentUserView: class  {
    func commentsWithSussess(commentModel: CommentModel)
    func commentsWithError(error: String)
}
