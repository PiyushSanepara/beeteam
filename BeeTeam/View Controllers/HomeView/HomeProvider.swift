//
//  HomeProvider.swift
//  BeeTeam
//

import Foundation

class HomeProvider {
    func getPost(next: String?, person: Int?, department: Int?, successHandler : @escaping(_ objects: PostModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.posts(next: next, person: person, department: department))
            .onSuccess { (response: PostModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getStory(next: String?, successHandler : @escaping(_ objects: StoryModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.stories(next: next))
            .onSuccess { (response: StoryModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func like(id: Int, isLike: Bool, successHandler : @escaping(_ objects: CommonModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.toggleLike(id: id, like: isLike))
            .onSuccess { (response: CommonModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func createComment(id: Int, In_reply_to: Int?, CommentString: String?, successHandler : @escaping(_ objects: CommentModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.createComment(id: id, In_reply_to: In_reply_to, CommentString: CommentString))
            .onSuccess { (response: CommentModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getStoryDetailData(id: Int, successHandler : @escaping(_ objects: StoryDetailModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        NetworkManager.makeRequest(HttpRouter.getStories(id: id))
            .onSuccess { (response: StoryDetailModel) in
                if response.success ?? false {
                    successHandler(response)
                } else {
                    errorHandler(response.errorMessage ?? "")
                }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getMyStoryData(next: String?, successHandler : @escaping(_ objects: StoryDetailModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        NetworkManager.makeRequest(HttpRouter.myStories(next: next))
            .onSuccess { (response: StoryDetailModel) in
                if response.success ?? false {
                    successHandler(response)
                } else {
                    errorHandler(response.errorMessage ?? "")
                }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
        
    func createStory(materialID: String?, text_Content: String?, successHandler : @escaping(_ objects: CommonModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.createStory(materialID: materialID, text_Content: text_Content))
            .onSuccess { (response: CommonModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func createPost(materialID: String?, text_Content: String?, postId: Int?, successHandler : @escaping(_ objects: CommonModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.createPost(materialID: materialID, text_Content: text_Content, postId: postId))
            .onSuccess { (response: CommonModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getUnreadCount(successHandler : @escaping(_ objects: Int) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        NetworkManager.makeRequest(HttpRouter.unreadCount)
            .onSuccess { (response: CommonModel) in
                if response.success ?? false {
                    successHandler(response.unreadCount ?? 0)
                } else {
                    errorHandler(response.errorMessage ?? "")
                }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
}
