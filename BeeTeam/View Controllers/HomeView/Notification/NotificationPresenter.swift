//
//  NotificationPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class NotificationPresenter: NSObject {
    let provider: NotificationProvider
    weak private var notificationView: NotificationView?
    
    init(provider: NotificationProvider) {
        self.provider = provider
    }
    
    func attachView(view: NotificationView?) {
        guard let view = view else { return }
        notificationView = view
    }
    
    func getNotifications() {
        provider.getNotifications(successHandler: { (response) in
            self.notificationView?.getNotificationsWithSussess(notificationModel: response)
        }) { (error) in
            self.notificationView?.getNotificationsWithError(error: error)
        }
    }
    
}
