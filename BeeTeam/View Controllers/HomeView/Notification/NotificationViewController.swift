//
//  NotificationViewController.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 25/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet var navigationTitleView: UIView!
    
    var notificationDataSource = [NotificationCustom]()
    
    let notificationPresenter = NotificationPresenter(provider: NotificationProvider())
    var nextData: String?
    var postId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
        self.notificationTableView.tableFooterView = UIView(frame: .zero)
        
        notificationPresenter.attachView(view: self)
        notificationPresenter.getNotifications()
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
        let markAllReadButton = UIBarButtonItem(title: "اقرأ كل شيء", style: .plain, target: self, action: #selector(markAllReadButtonAction))

        self.navigationItem.rightBarButtonItem = markAllReadButton
    }
    
//    MARK: ACTION
    @objc func markAllReadButtonAction() {
        SignalRClientManager.shared.notificationReadAll { (isCompleted) in
            if isCompleted {
                self.notificationPresenter.getNotifications()
            }
        }
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as? NotificationTableViewCell
        cell?.setContent(notification: notificationDataSource[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension NotificationViewController: NotificationView {
    func getNotificationsWithSussess(notificationModel: NotificationModel) {
        self.notificationDataSource = notificationModel.notifications ?? []
        
        self.notificationTableView.reloadData()
    }
    
    func getNotificationsWithError(error: String) {
        self.notificationDataSource.removeAll()
        self.notificationTableView.reloadData()
        
        alertShowWithOK(message: error, viewController: self)
    }
}
