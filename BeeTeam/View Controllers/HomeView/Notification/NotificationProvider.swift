//
//  NotificationProvider.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class NotificationProvider {
    func getNotifications(successHandler : @escaping(_ objects: NotificationModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.notifications)
            .onSuccess { (response: NotificationModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
}
