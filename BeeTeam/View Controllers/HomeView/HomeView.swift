//
//  HomeView.swift
//  BeeTeam
//

import Foundation
import ObjectMapper

protocol HomeView: class  {
    func postWithSussess(postModel: PostModel)
    func postWithError(error: String)
    
    func storyWithSussess(storyModel: StoryModel)
    func storyWithError(error: String)
    
    func likeWithSussess(commonModel: CommonModel)
    func likeWithError(error: String)
    
    func createCommentWithSussess(commentModel: CommentModel)
    func createCommentWithError(error: String)
    
    func getStoryDetailWithSussess(storyDetailModel: StoryDetailModel)
    func getStoryDetailWithWithError(error: String)
    
    func getMyStoryWithSussess(storyDetailModel: StoryDetailModel)
    func getMyStoryWithWithError(error: String)
    
    func createStoryWithSussess(commonModel: CommonModel)
    func createStoryWithError(error: String)
    
    func createPostWithSussess(commonModel: CommonModel)
    func createPostWithError(error: String)
    
    func unreadCountWithSussess(unreadCount: Int)
    func unreadCountWithError(error: String)
}
