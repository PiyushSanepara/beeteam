//
//  MaterialCollectionViewCell.swift
//  BeeTeam
//
//  Created by CSL Piyush on 06/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import ImageViewer_swift

protocol PlayVideoDelegate: class {
    func palyPostVideo(sender: UIButton)
}

class MaterialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    
    var videoDelegate: PlayVideoDelegate?
    var material: Materials?
//    var photosList = [AXPhotoProtocol]()
    
    func setPostData(materialData: Materials) {
        material = materialData
        
        if materialData.type == MaterialType.Image.rawValue, let id = materialData.id, let url = URL(string: UrlConstant.baseURLMaterialPost + "?id=\(id)") {
            DispatchQueue.main.async {
                self.postImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_logo_placeholder"))
                self.postImage.setupImageViewer()
            }
        } else if materialData.type == MaterialType.Video.rawValue, let id = materialData.id, let url = URL(string: UrlConstant.baseURLVideoThumbnail + "?id=\(id)") {
            DispatchQueue.main.async {
                self.postImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_logo_placeholder"))
            }
        } else {
            DispatchQueue.main.async {
                self.postImage.image = UIImage(named: "ic_logo_placeholder")
            }
        }
        
        btnPlay.isHidden = materialData.type == MaterialType.Video.rawValue ? false : true
    }
    
    @IBAction func btnPlayVideoTapped(_ sender: UIButton) {
        let rootController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        if let videoURL = URL(string: UrlConstant.baseURLMaterialPost + "?id=\(material?.id ?? 0)") {
            let headers: [String: String] = [
                HeaderConstant.authorization: UserDefault.getToken()
            ]
            let asset = AVURLAsset(url: videoURL, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
            let playerItem = AVPlayerItem(asset: asset)
            let player = AVPlayer(playerItem: playerItem)
            
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            rootController?.visibleViewController?.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
}
