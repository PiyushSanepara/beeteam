//
//  StoriesCollectionViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 10/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol StoriesCollectionViewCellDelegate {
    func addStoryAction(cell: StoriesCollectionViewCell?)
}

class StoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userImageLabel: UIImageView!
    @IBOutlet var viewBorder: UIView!
    @IBOutlet weak var btnAddStory: UIButton!
    @IBOutlet weak var imageAddStory: UIImageView!
    
    var delegate: StoriesCollectionViewCellDelegate!
    
    func updateBorder(color: String) {
        viewBorder.layer.borderColor = UIColor.hexStringToUIColor(hex: color).cgColor
        viewBorder.layer.borderWidth = 3
    }
    
    @IBAction func addStoryAction(_ sender: Any) {
        if let del = self.delegate {
            del.addStoryAction(cell: self)
        }
    }
    
}
