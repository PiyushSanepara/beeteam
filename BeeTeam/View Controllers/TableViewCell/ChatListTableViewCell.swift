//
//  ChatListTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var chatLastMessageLabel: UILabel!
    @IBOutlet weak var statusIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(chat: ChatList) {
        
        if let id = chat.employee?.id, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.userImageView.setSDImage(url: url, placeHolderImage: UIImage(named: "chatSinglePlaceholder"))
            }
        } else {
            self.userImageView.image = UIImage(named: "chatSinglePlaceholder")
        }
        
        self.userNameLabel.text = chat.employee?.arFullName
        self.chatLastMessageLabel.text = chat.lastMessage?.messageText?.utf8DecodedString()
        
        if chat.lastMessage?.readDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "seen-icon")
        } else if chat.lastMessage?.receivedDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "received-icon")
        } else if chat.lastMessage?.creationDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "sent-icon")
        } else {
            self.statusIconImageView.image = UIImage(named: "waiting-icon")
        }
    }
    
    func setGroupData(chat: ChatList) {
        
        if let id = chat.groupId, let url = URL(string: UrlConstant.baseURLGroupPicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.userImageView.setSDImage(url: url, placeHolderImage: UIImage(named: "chatMultiplePlaceholder"))
            }
        } else {
            self.userImageView.image = UIImage(named: "chatMultiplePlaceholder")
        }
        
        self.userNameLabel.text = chat.groupHeader
        self.chatLastMessageLabel.text = chat.lastMessage?.messageText?.utf8DecodedString()
        
        if chat.lastMessage?.readDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "seen-icon")
        } else if chat.lastMessage?.receivedDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "received-icon")
        } else if chat.lastMessage?.creationDateTime != nil {
            self.statusIconImageView.image = UIImage(named: "sent-icon")
        } else {
            self.statusIconImageView.image = UIImage(named: "waiting-icon")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
