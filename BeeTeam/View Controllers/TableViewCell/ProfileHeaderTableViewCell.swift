//
//  ProfileHeaderTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 12/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol ProfileHeaderTableViewCellDelegate {
    func textDidEndEditing(cell: ProfileHeaderTableViewCell, textContent: String?)
    func editProfileImageTapped(cell: ProfileHeaderTableViewCell)
}

class ProfileHeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var positionLabel: UILabel!
    @IBOutlet var empIdLabel: UILabel!
    @IBOutlet weak var whatsOnYourMindTextfield: UITextField!
    @IBOutlet weak var StackViewBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var btnEditProfileImage: UIButton!
    
    var delegate: ProfileHeaderTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.whatsOnYourMindTextfield.delegate = self
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            userImageView.layer.cornerRadius = 100
        } else {
            userImageView.layer.cornerRadius = 60
        }
        
    }
    
    func updateProfileData(profileData: ProfileModel?) {
        if let id = profileData?.id, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
        DispatchQueue.main.async {
            self.userImageView.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
        }
            self.empIdLabel.text = "ID: \(id)"
        }
        self.usernameLabel.text = profileData?.arFullName
        self.positionLabel.text = profileData?.arPosition
        
        if profileData?.id == UserDefault.getLoginDetails()?.employee?.id {
            self.whatsOnYourMindTextfield.isHidden = false
            StackViewBottomConstraints.constant = 104
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(openGalleryOption))
            self.userImageView.isUserInteractionEnabled = true
            self.userImageView.addGestureRecognizer(tapGestureRecognizer)
            self.btnEditProfileImage.isHidden = false
        } else {
            self.whatsOnYourMindTextfield.isHidden = true
            StackViewBottomConstraints.constant = 32
            self.btnEditProfileImage.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func editProfileImageTapped(_ sender: UIButton) {
        openGalleryOption()
    }
    
    @objc func openGalleryOption() {
        delegate?.editProfileImageTapped(cell: self)
    }

}

extension ProfileHeaderTableViewCell: UITextFieldDelegate {
//    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        if let del = delegate {
//            del.textDidEndEditing(cell: self, textContent: textField.text)
//            textField.text = nil
//            textField.resignFirstResponder()
//        }
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 else {
            return true
        }
        
        delegate?.textDidEndEditing(cell: self, textContent: textField.text)
        textField.text = nil
//            textField.resignFirstResponder()
        return true
    }
}
