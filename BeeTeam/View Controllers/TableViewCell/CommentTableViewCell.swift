//
//  CommentTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate {
    func replyButton(section: Int)
    func cancelButtonAction(cell: CommentTableViewCell)
    func replyButtonAction(cell: CommentTableViewCell)
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: TitleLabel!
    @IBOutlet weak var userDateLabel: UILabel!
    @IBOutlet weak var userCommetLabel: TitleLabel!
    
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var replyCommentView: UIView!
    
    var delegate: CommentTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setContent(comment: Comment) {
        if let id = comment.empId, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.userImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
        
        userNameLabel.text = comment.arFullName
        userDateLabel.text = comment.commentDate?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.timeAgo()
        userCommetLabel.text = comment.textContent
        
        replyCommentView.isHidden = comment.isHideReplyView
    }
    
    @IBAction func replyButton(_ sender: UIButton) {
        delegate?.replyButton(section: sender.tag)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        delegate?.cancelButtonAction(cell: self)
    }
    
    @IBAction func replyButtonAction(_ sender: UIButton) {
        delegate?.replyButtonAction(cell: self)
    }
}
