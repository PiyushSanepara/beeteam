//
//  CallTableViewCell.swift
//  BeeTeam
//
//  Created by CSL Piyush on 17/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol CallTableViewCellDelegate {
    func callButtonAction(index: Int)
}

class CallTableViewCell: UITableViewCell {

//    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var callDateTimeLabel: UILabel!
    @IBOutlet weak var statusIconImageView: UIImageView!
    @IBOutlet weak var callTypeButton: UIButton!
    
    var delegate: CallTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(call: Call) {
        self.userNameLabel.text = call.receiver?.arFullName
        self.callDateTimeLabel.text = call.callDateTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.string(withFormat: ConstantFormatter.DateFormatFull)
        
        switch call.callStatus {
        case 1:
            self.statusIconImageView.image = #imageLiteral(resourceName: "Status1.png")
        case 2,7:
            self.statusIconImageView.image = #imageLiteral(resourceName: "Status-2-7.png")
        case 3,4:
            self.statusIconImageView.image = #imageLiteral(resourceName: "Status-3-4.png")
        case 5,6:
            self.statusIconImageView.image = #imageLiteral(resourceName: "Status-5-6.png")
        case 8,9:
            self.statusIconImageView.image = (call.caller?.id == UserDefault.getLoginDetails()?.employee?.id) ? #imageLiteral(resourceName: "Status-8-9-outgoing.png") : #imageLiteral(resourceName: "Status-8-9-incoming.png")
        default:
            self.statusIconImageView.image = nil
        }
        
        if #available(iOS 13.0, *) {
            self.callTypeButton.setImage(((call.callType ?? 0) == 2) ? UIImage(systemName: "video.fill") : UIImage(systemName: "phone.fill"), for: .normal)
        } else {
            self.callTypeButton.setImage(((call.callType ?? 0) == 2) ? #imageLiteral(resourceName: "icon-video-call.png") : #imageLiteral(resourceName: "icon-voice-call.png"), for: .normal)
        }
        
    }

    @IBAction func callButtonAction(_ sender: UIButton) {
        delegate?.callButtonAction(index: sender.tag)
    }
}
