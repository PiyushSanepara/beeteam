//
//  NotificationTableViewCell.swift
//  BeeTeam
//
//  Created by CSL Piyush on 02/09/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationTextLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setContent(notification: NotificationCustom) {
        if let id = notification.actionMaker?.id, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.userImageView.setSDImage(url: url, placeHolderImage: #imageLiteral(resourceName: "ic_user_placeholder"))
            }
        } else {
            self.userImageView.image = #imageLiteral(resourceName: "ic_user_placeholder")
        }
        
        self.notificationTextLabel.text = notification.notificationText
        self.dateLabel.text = notification.creationTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.timeAgo()
        
        self.backgroundColor = (notification.isRead ?? false) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.8705882353, green: 0.7803921569, blue: 0.7411764706, alpha: 1)
    }
}
