//
//  MyRequestTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 12/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class MyRequestTableViewCell: UITableViewCell {

    @IBOutlet var numberOfRequestLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class MyRequestBottomTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
