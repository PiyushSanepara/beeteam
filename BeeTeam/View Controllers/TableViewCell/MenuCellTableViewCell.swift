//
//  MenuCellTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class MenuCellTableViewCell: UITableViewCell {

    @IBOutlet var menuImage: UIImageView!
    @IBOutlet weak var lblMenuTitle: UILabel!
    @IBOutlet var lblLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
