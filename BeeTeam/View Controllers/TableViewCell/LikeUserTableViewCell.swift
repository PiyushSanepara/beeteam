//
//  LikeUserTableViewCell.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 25/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class LikeUserTableViewCell: UITableViewCell {
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateLikeUserData(data: Employee) {
        if let id = data.id, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.imgProfile.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        } else {
            self.imgProfile.image = UIImage(named: "ic_user_placeholder")
        }
        self.lblName.text = data.arFullName
    }
    
}
