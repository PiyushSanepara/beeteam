//
//  CellDetail2TableViewCell.swift
//  BeeTeam
//
//  Created by CSL Piyush on 17/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol CellDetailTableViewCellDelegate {
    func callButtonAction()
    func videoCallButtonAction()
}
class CellDetail2TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    var delegate: CellDetailTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func callButtonAction(_ sender: Any) {
        delegate?.callButtonAction()
    }
    
    @IBAction func videoCallButtonAction(_ sender: Any) {
        delegate?.videoCallButtonAction()
    }
}
