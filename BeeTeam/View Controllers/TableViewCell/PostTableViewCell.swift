
//
//  PostTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 12/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import SDWebImage

protocol PostTableViewCellDelegate {
    func likeAction(index: Int, isLiked: Bool, likeCount: Int)
    func profileImageTapped(index: Int)
    func showLikeBtnTapped(index: Int)
    func showCommentBtnTapped(index: Int)
    func shareAction(index: Int)
}

class PostTableViewCell: UITableViewCell {

    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var usernameLabel: TitleLabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var commentTextfield: UITextField!
    @IBOutlet var latestCommentLabel: UILabel!
    @IBOutlet var targetAudienceImageView: UIImageView!
    @IBOutlet var textContentLabel: UILabel!
    @IBOutlet var collectionMaterials: UICollectionView!
    @IBOutlet var pageControll: UIPageControl!
    @IBOutlet var heightCollectionMaterials: NSLayoutConstraint!
    @IBOutlet var btnLikeCount: UIButton!
    @IBOutlet weak var latestCommentBottomConstraint: NSLayoutConstraint!
    
    //For basePost:
    @IBOutlet var basePostView: UIView!
    @IBOutlet var materialBottonTolikeSectionConstraint: NSLayoutConstraint!
    @IBOutlet var baseProfileImageView: UIImageView!
    @IBOutlet var baseUsernameLabel: TitleLabel!
    @IBOutlet var baseDateLabel: UILabel!
    @IBOutlet var baseTargetAudienceImageView: UIImageView!
    @IBOutlet var baseTextContentLabel: UILabel!
    @IBOutlet var baseCollectionMaterials: UICollectionView!
    @IBOutlet var basePageControll: UIPageControl!
    @IBOutlet var baseHeightCollectionMaterials: NSLayoutConstraint!
    
    var postData: [Materials] = []
    var basePostData: [Materials] = []
    
    var delegate: PostTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if UIDevice.current.userInterfaceIdiom == .pad {
            profileImageView.layer.cornerRadius = 40
            baseProfileImageView.layer.cornerRadius = 40
        } else {
            profileImageView.layer.cornerRadius = 30
            baseProfileImageView.layer.cornerRadius = 30
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
        self.profileImageView.addGestureRecognizer(tapGestureRecognizer)
        
        let labelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped(tapGestureRecognizer:)))
        self.usernameLabel.addGestureRecognizer(labelTapGestureRecognizer)
        
        collectionMaterials.register(UINib(nibName: "MaterialCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MaterialCollectionViewCell")
    }
    
    func updatePostData(post: Post) {
        postData = post.materials ?? []
        
        basePostView.isHidden = (post.basePost == nil)
        materialBottonTolikeSectionConstraint.isActive = (post.basePost == nil)
        
        if let id = post.empId, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.profileImageView.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
        
        self.usernameLabel.text = post.arFullName
        self.dateLabel.text = post.publishDateTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.timeAgo()
        
        if post.likesCount ?? 0 > 0 {
            let likeText = (post.likesCount ?? 0 > 1) ? "\(post.likesCount ?? 0) likes" : "\(post.likesCount ?? 0) like"
            self.btnLikeCount.setTitle(likeText, for: .normal)
//            self.heightConstraintLikeCount.constant = 20
        } else {
            self.btnLikeCount.setTitle("", for: .normal)
//            self.heightConstraintLikeCount.constant = 0
        }
        
        self.likeButton.isSelected = post.liked
        
        if post.commentsCount ?? 0 > 0 {
            self.commentButton.setTitle(String(post.commentsCount ?? 0), for: .normal)
        } else {
            self.commentButton.setTitle("", for: .normal)
        }
        
        if post.shareCount ?? 0 > 0 {
            self.shareButton.setTitle(String(post.shareCount ?? 0), for: .normal)
        } else {
            self.shareButton.setTitle("", for: .normal)
        }
        
        self.textContentLabel.text = post.textContent
        
        if post.topComments.count > 0 {
            
            let commentator = "\(post.topComments.last?.arFullName ?? ""): "

            let attributedString = NSMutableAttributedString(string:commentator, attributes:[NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)])
            
            let comment = NSMutableAttributedString(string:post.topComments.first?.textContent ?? "")
            
            attributedString.append(comment)
                
            let labelCommentTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCommentBtnTapped(tapGestureRecognizer:)))
            self.latestCommentLabel.addGestureRecognizer(labelCommentTapGestureRecognizer)

            self.latestCommentLabel.attributedText = attributedString
            
            self.latestCommentBottomConstraint.constant = 16
        } else {
            self.latestCommentLabel.text = nil
            self.latestCommentBottomConstraint.constant = 0
        }
        
        self.pageControll.numberOfPages = postData.count
        
        if postData.count == 0 {
            heightCollectionMaterials.constant = 0
        }else {
            heightCollectionMaterials.constant = 180
        }
        
        DispatchQueue.main.async {
            self.collectionMaterials.reloadData()
        }
        
                
        //        if let videoURL = URL(string: "https://ob.navjodev.com/api/v1/post/material?id=1056") {
        //            let headers: [String: String] = [
        //               HeaderConstant.authorization: authantication
        //            ]
        //            let asset = AVURLAsset(url: videoURL, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
        //            let playerItem = AVPlayerItem(asset: asset)
        //            let player = AVPlayer(playerItem: playerItem)
        //
        //
        //            let playerLayer = AVPlayerLayer(player: player)
        //            playerLayer.frame = self.view.bounds
        //            cell.postVideoView.layer.addSublayer(playerLayer)
        //            player.play()
        //        }
                
        if let bPost = post.basePost {
            self.updateSharedPostData(post: bPost)
        }
    }
    
    func updateSharedPostData(post: Post) {
        basePostData = post.materials ?? []
        
        if let id = post.empId, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.baseProfileImageView.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
        
        self.baseUsernameLabel.text = post.arFullName
        self.baseDateLabel.text = post.publishDateTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.timeAgo()
                
        self.baseTextContentLabel.text = post.textContent
                
        self.basePageControll.numberOfPages = basePostData.count
        
        if basePostData.count == 0 {
            baseHeightCollectionMaterials.constant = 0
        }else {
            baseHeightCollectionMaterials.constant = 180
        }
        
        DispatchQueue.main.async {
            self.baseCollectionMaterials.reloadData()
        }
    }
    
    @objc func profileImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView
        delegate?.profileImageTapped(index: profileImageView.tag)
    }
    
    @objc func showCommentBtnTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.showCommentBtnTapped(index: commentButton.tag)
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
        var count = Int(sender.accessibilityLabel ?? "0") ?? 0
        count = sender.isSelected ? (count - 1) : (count + 1)
        
        sender.isSelected = !sender.isSelected
        
        if count > 0 {
            let likeText = (count > 1) ? "\(count) likes" : "\(count) like"
            self.btnLikeCount.setTitle(likeText, for: .normal)
//            self.heightConstraintLikeCount.constant = 20
        } else {
            self.btnLikeCount.setTitle("", for: .normal)
//            self.heightConstraintLikeCount.constant = 0
        }
        self.likeButton.accessibilityLabel = String(count)
        delegate?.likeAction(index: sender.tag, isLiked: sender.isSelected, likeCount: count)
    }
    
    @IBAction func showLikeTapped(_ sender: UIButton) {
        delegate?.showLikeBtnTapped(index: sender.tag)
    }
    
    @IBAction func showCommentBtnTapped(_ sender: UIButton) {
        delegate?.showCommentBtnTapped(index: sender.tag)
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        delegate?.shareAction(index: sender.tag)
    }
}

extension PostTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionMaterials:
            return postData.count
        case self.baseCollectionMaterials:
            return basePostData.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let materialCell = collectionMaterials.dequeueReusableCell(withReuseIdentifier: "MaterialCollectionViewCell", for: indexPath) as? MaterialCollectionViewCell
        if collectionView == self.collectionMaterials {
            if postData.count > indexPath.row {
                materialCell?.setPostData(materialData: postData[indexPath.row])
            }
        } else {
            if basePostData.count > indexPath.row {
                materialCell?.setPostData(materialData: basePostData[indexPath.row])
            }
        }
        
        return materialCell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let materialCell = collectionMaterials.dequeueReusableCell(withReuseIdentifier: "MaterialCollectionViewCell", for: indexPath) as? MaterialCollectionViewCell
//        
//        if postData[indexPath.row].type == "Image" {
//            if let photoList = materialCell?.photosList {
//                let dataSource = AXPhotosDataSource(photos: photoList, initialPhotoIndex: indexPath.row)
//                let photosViewController = AXPhotosViewController(dataSource: dataSource)
//                self.present(photosViewController, animated: true, completion: nil)
//            }
//        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionMaterials {
            self.pageControll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        } else if scrollView == baseCollectionMaterials {
            self.basePageControll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
    }

}
