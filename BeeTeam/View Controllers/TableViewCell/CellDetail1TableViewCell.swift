//
//  CellDetail1TableViewCell.swift
//  BeeTeam
//
//  Created by CSL Piyush on 17/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CellDetail1TableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
