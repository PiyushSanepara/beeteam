//
//  CommentReplyTableViewCell.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 30/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CommentReplyTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: TitleLabel!
    @IBOutlet weak var userDateLabel: UILabel!
    @IBOutlet weak var userCommetLabel: TitleLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setContent(comment: Comment?) {
        guard let comment = comment else { return }
        
        if let id = comment.empId, let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
            DispatchQueue.main.async {
                self.userImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
        
        userNameLabel.text = comment.arFullName
        userDateLabel.text = comment.commentDate?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.timeAgo()
        userCommetLabel.text = comment.textContent
    }

}
