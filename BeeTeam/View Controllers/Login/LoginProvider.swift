//
//  LoginProvider.swift
//  BeeTeam
//

import Foundation

class LoginProvider {
    func loginUser(empId: String, empPassword: String, successHandler : @escaping(_ objects: LoginModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.loginUser(empId: empId, empPass: empPassword), isShowProgress: true)
            .onSuccess { (response: LoginModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "Wrong Credentials")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
}
