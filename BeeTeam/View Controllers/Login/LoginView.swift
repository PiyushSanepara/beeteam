//
//  LoginView.swift
//  BeeTeam
//

import Foundation
import ObjectMapper

protocol LoginView: class  {
    func loginWithSussess(loginModel: LoginModel)
    func loginWithError(error: String)
}
