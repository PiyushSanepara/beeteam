//
//  LoginPresenter.swift
//  BeeTeam
//

import Foundation


class LoginPresenter: NSObject {
    let provider: LoginProvider
    weak private var loginView: LoginView?
    
    init(provider: LoginProvider) {
        self.provider = provider
    }
    
    func attachView(view: LoginView?) {
        guard let view = view else { return }
        loginView = view
    }
    
    func loginUser(empId: String, empPassword: String) {
        provider.loginUser(empId: empId, empPassword: empPassword, successHandler: { (response) in
            self.loginView?.loginWithSussess(loginModel: response)
        }) { (error) in
            self.loginView?.loginWithError(error: error)
        }
    }
}
