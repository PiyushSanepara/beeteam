//
//  LoginViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 09/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, SSASideMenuDelegate {
    
    let loginPresenter = LoginPresenter(provider: LoginProvider())
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        loginPresenter.attachView(view: self)
        self.checkAlreadyLogin()
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func checkAlreadyLogin() {
        if (UserDefault.getLoginDetails() != nil) {
//            self.openDashboard()
            guard let empId = UserDefault.getLoginDetails()?.employee?.id else { return }
            guard UserDefault.getPassword() != "" else { return }
            loginPresenter.loginUser(empId: String(empId), empPassword: UserDefault.getPassword())
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if(txtUserName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! == 0 {
            self.showWaring(message: "Please Enter Username")
        } else if(txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! == 0 {
            self.showWaring(message: "Please Enter Password")
        } else {
            loginPresenter.loginUser(empId: txtUserName.text ?? "", empPassword: txtPassword.text ?? "")
            //loginPresenter.loginUser(empId: "18009", empPassword: "123")        }
        }
    }
    
    func showWaring(title: String = "BeeTeam", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openDashboard() {
        var sideMenu = SSASideMenu()
        let menuViewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SideMenuDashboardViewController") as? SideMenuDashboardViewController
        
        let storyBoard = UIStoryboard(name: "Home", bundle: nil)
        let homeViewController : HomeViewController = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let navigationController : NavigationController = NavigationController(rootViewController: homeViewController)
        
        navigationController.sideMenu = sideMenu
        sideMenu = SSASideMenu(contentViewController: navigationController, rightMenuViewController: menuViewController!)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: false, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 1.0))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 10.0))
        sideMenu.delegate = self
        sideMenu.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(sideMenu, animated: true)
    }
    
}

extension LoginViewController: LoginView {
    func loginWithSussess(loginModel: LoginModel) {
        if loginModel.success ?? false {
//            print(loginModel.authentication)
            UserDefault.setToken(token: loginModel.authentication ?? "")
            UserDefault.setLoginDetails(loginDetails: loginModel)
            if UserDefault.getPassword().count > 0 {
                UserDefault.setPassword(passwordString: UserDefault.getPassword())
            } else {
                UserDefault.setPassword(passwordString: txtPassword.text ?? "")
            }
            
            if UserDefault.getLoginDetails() != nil {
                SignalRClientManager.shared.initialEvents { (isConnected) in
                    print("status for callback", isConnected)
                }
            }
            self.openDashboard()
        }
    }
    
    func loginWithError(error: String) {
        alertShowWithOK("BeeTeam", message: error, viewController: self)
    }
}
