//
//  MyRequestViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 12/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class MyRequestViewController: UIViewController {

    @IBOutlet var navigationTitleView: UIButton!
    
    @IBOutlet var requestTableview: UITableView!
    
    var requestArray: [Int] = [1]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configNavigationBar()
    }
    
    func configNavigationBar() {
        self.navigationItem.titleView = self.navigationTitleView
    }
    
    @IBAction func AddRequestAction(_ sender: Any) {
        requestTableview.beginUpdates()
        requestArray.append((requestArray.last ?? 0) + 1)
        let indexPath:IndexPath = IndexPath(row:(self.requestArray.count - 1), section:0)
        requestTableview.insertRows(at: [indexPath], with: .left)
        requestTableview.endUpdates()
    }
    
}

extension MyRequestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return requestArray.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRequestTableViewCell") as! MyRequestTableViewCell
            
            cell.numberOfRequestLabel.text = "\(self.requestArray[indexPath.row])"
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyRequestBottomTableViewCell") as? MyRequestBottomTableViewCell ?? UITableViewCell()
            
            return cell
        }
    }
}
