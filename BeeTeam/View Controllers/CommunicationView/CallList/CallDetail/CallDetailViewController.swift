//
//  CallDetailViewController.swift
//  BeeTeam
//
//  Created by CSL Piyush on 17/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CallDetailViewController: UIViewController {
    
    @IBOutlet weak var callDetailTableView: UITableView!
    
    @IBOutlet var navigationTitleView: UIView!
    
    var call: Call?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callDetailTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
    }
    
}

extension CallDetailViewController: UITableViewDelegate, UITableViewDataSource, CellDetailTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row != 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellDetail1TableViewCell") as? CellDetail1TableViewCell
            
            switch indexPath.row {
            case 0:
                cell?.titleLabel.text = "دعوة صادرة إلى"//Outgoing Call To
                cell?.detailLabel.text = call?.receiver?.arFullName
            case 1:
                cell?.titleLabel.text = "وقت النداء"//Call Time
                cell?.detailLabel.text = call?.callDateTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.string(withFormat: ConstantFormatter.DateFormatFull) ?? "-"
            case 2:
                cell?.titleLabel.text = "المدة الزمنية"//Duration
                cell?.detailLabel.text = getTimeDifference(fromDate: call?.startTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer) ?? Date(), toDate: call?.endTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer) ?? Date())
            case 3:
                cell?.titleLabel.text = "وقت البدء"//Start Time
                cell?.detailLabel.text = call?.startTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.string(withFormat: ConstantFormatter.DateFormatFull) ?? "-"
            case 4:
                cell?.titleLabel.text = "وقت النهاية"//End Time
                cell?.detailLabel.text = call?.endTime?.convertToDateTime(for: ConstantFormatter.DateFormatFullServer)?.string(withFormat: ConstantFormatter.DateFormatFull) ?? "-"
            case 5:
                cell?.titleLabel.text = "حالة المكالمة"//Call Status
                cell?.detailLabel.text = call?.callStatus?.getCallStatus()
            case 6:
                cell?.titleLabel.text = "نوع الاتصال"
                cell?.detailLabel.text = call?.callType?.getCallType()
            default:
                break
            }
            
            return cell ?? UITableViewCell()
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellDetail2TableViewCell") as? CellDetail2TableViewCell
            cell?.titleLabel.text = "أتصل مرة أخرى"
            cell?.delegate = self
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "showChatFromTelecom", sender: indexPath)
    }
    
    func callButtonAction() {
        var otherPartyId: Int?
        if self.call?.receiver?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.call?.receiver?.id
        } else if self.call?.caller?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.call?.caller?.id
        }
        
        SignalRClientManager.shared.isAudioCall = true
        SignalRClientManager.shared.initiateCallWithOption(callOption: [otherPartyId ?? 0, true])
    }
    
    func videoCallButtonAction() {
        var otherPartyId: Int?
        if self.call?.receiver?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.call?.receiver?.id
        } else if self.call?.caller?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.call?.caller?.id
        }
        
        SignalRClientManager.shared.isAudioCall = false
        SignalRClientManager.shared.initiateCallWithOption(callOption: [otherPartyId ?? 0, true])
    }
}
