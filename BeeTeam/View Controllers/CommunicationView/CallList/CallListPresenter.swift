//
//  CallListPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class CallListPresenter: NSObject {
    let provider: CallListProvider
    weak private var callView: CallListView?
    
    init(provider: CallListProvider) {
        self.provider = provider
    }
    
    func attachView(view: CallListView?) {
        guard let view = view else { return }
        callView = view
    }
    
    func getCallList(next: String?) {
        provider.getCallList(next: next, successHandler: { (response) in
            self.callView?.callListWithSussess(callModel: response)
        }) { (error) in
            self.callView?.callListWithError(error: error)
        }
    }
    
}
