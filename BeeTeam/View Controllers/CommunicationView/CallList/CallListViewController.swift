//
//  CallListViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CallListViewController: UIViewController {

    @IBOutlet var callTableview: UITableView!
    
    let callPresenter = CallListPresenter(provider: CallListProvider())
    var callDataSource = [Call]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callPresenter.attachView(view: self)
        callTableview.tableFooterView = UIView()
        
        callPresenter.getCallList(next: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCallDetailFromCall" {
            if let vc = segue.destination as? CallDetailViewController, let indexPath = sender as? IndexPath {
                vc.call = callDataSource[indexPath.row]
            }
        }
    }
    
}

extension CallListViewController: UITableViewDelegate, UITableViewDataSource, CallTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.callDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallTableViewCell") as? CallTableViewCell
        cell?.delegate = self
        cell?.callTypeButton.tag = indexPath.row
        cell?.setData(call: callDataSource[indexPath.row])
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showCallDetailFromCall", sender: indexPath)
    }
    
    func callButtonAction(index: Int) {
        var otherPartyId: Int?
        if self.callDataSource[index].receiver?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.callDataSource[index].receiver?.id
        } else if self.callDataSource[index].caller?.id != UserDefault.getLoginDetails()?.employee?.id {
            otherPartyId = self.callDataSource[index].caller?.id
        }
        SignalRClientManager.shared.isAudioCall = (self.callDataSource[index].callType == 2) ? false : true
        SignalRClientManager.shared.initiateCallWithOption(callOption: [otherPartyId ?? 0, true])
    }
}

extension CallListViewController: CallListView {
    func callListWithSussess(callModel: CallModel) {
        self.callDataSource = callModel.callList ?? []
        self.callTableview.reloadData()
    }
    
    func callListWithError(error: String) {
        self.callDataSource.removeAll()
        self.callTableview.reloadData()
        
        alertShowWithOK(message: error, viewController: self)
    }
    
}
