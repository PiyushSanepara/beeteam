//
//  ReceiverRingViewController.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 29/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import AVKit

class ReceiverRingViewController: UIViewController {
    @IBOutlet weak var imgCaller: UIImageView!
    @IBOutlet weak var lblCallerName: UILabel!
    var callId: Int?
    var callerData: Employee?
    var isAudioCall: Bool = true
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.player?.stop()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadCallerData(caller: callerData)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.playRing(ringType: RingType.incoming.rawValue)
    }
    
    func loadCallerData(caller: Employee?) {
        SignalRClientManager.shared.delegate = self
        self.lblCallerName.text = callerData?.arFullName
        if let id = callerData?.id {
            if let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
                DispatchQueue.main.async {
                    self.imgCaller.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_user_placeholder"))
                }
            } else {
                self.imgCaller.image = UIImage(named: "ic_user_placeholder")
            }
        }
        SignalRClientManager.shared.registerCallRinging(callData: [callId ?? 0])
    }
    
    
    @IBAction func btnAcceptCallTapped(_ sender: UIButton) {
        isAudioCall = sender.tag == 0 ? false : true
        SignalRClientManager.shared.acceptCall(callOption: [callId ?? 0])
    }
    
    @IBAction func btnEndCallTapped(_ sender: UIButton) {
        self.playRing(ringType: RingType.disconnected.rawValue, numberOfLoop: 0)
        SignalRClientManager.shared.endCalls { (_ ended) in
        }
    }
}

extension ReceiverRingViewController: SignalRManagerDelegate {
    func callIncomingChangeStatus(status: Int) {
        if self.navigationController?.topViewController?.isKind(of: ReceiverRingViewController.self) ?? false {
            switch status {
            case CallStatus.notReachable.rawValue, CallStatus.endedRingingSelf.rawValue, CallStatus.endedRingingOther.rawValue, CallStatus.callFailed.rawValue, CallStatus.notAnswer.rawValue :
                self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
    
    
    func callIsNowOngoing(callId: Int, caller: Employee?, receiver: Employee?, roomId: String) {
        if let vc = CommonMethods.viewController(name: .communication, identifier: "VideoViewController") as? VideoViewController {
            vc.roomId = roomId
            vc.callId = callId
            vc.callData = caller
            vc.isAudioCall = isAudioCall
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: PlayRing
extension ReceiverRingViewController {
    func playRing(ringType: String, numberOfLoop: Int = -1) {
        guard let url = Bundle.main.url(forResource: ringType, withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }
            player.numberOfLoops = numberOfLoop
            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
}
