//
//  VideoViewController.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 29/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import JitsiMeet

class VideoViewController: UIViewController {
    
    @IBOutlet weak var callView: JitsiMeetView!
    var callId: Int?
    var roomId: String?
    var callData: Employee?
    var isAudioCall: Bool = true
    var pingCallTimer: Timer? = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " "
        self.configView()
    }
    
    func configView() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        SignalRClientManager.shared.delegate = self
        callView.delegate = self
        let options: JitsiMeetConferenceOptions = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.serverURL = URL(string: "https://comm.ob-agency.com")
            builder.room = self.roomId
            builder.audioOnly = self.isAudioCall
            builder.audioMuted = !(self.isAudioCall)
            builder.videoMuted = self.isAudioCall
            builder.welcomePageEnabled = false
            builder.userInfo = JitsiMeetUserInfo(displayName: self.callData?.arFullName, andEmail: nil, andAvatar: nil)
        }
        callView.join(options)
        pingCallTimer = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(doCallPing), userInfo: nil, repeats: true)
    }
    
    @objc func doCallPing() {
        SignalRClientManager.shared.pingCallOnGoing(callData: [callId ?? 0])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if pingCallTimer != nil {
            pingCallTimer?.invalidate()
            pingCallTimer = nil
        }
    }
}

extension VideoViewController: JitsiMeetViewDelegate {
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
    }
    
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        SignalRClientManager.shared.endCalls { (_ isEnded) in
            self.callView.leave()
        }
    }
    
    func conferenceWillJoin(_ data: [AnyHashable : Any]!) {
        print("conferenceWillJoin")
    }
    
    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        print("enterPicture")
    }
}

extension VideoViewController: SignalRManagerDelegate {
    func callOutgoingChangeStatus(status: Int) {
        if status == CallStatus.endedCallBySelf.rawValue || status == CallStatus.callFailed.rawValue || status == CallStatus.endedCallByOther.rawValue {
            callView.leave()
            pushBackToTelecomViewController()
        }
    }
    
    func callIncomingChangeStatus(status: Int) {
        if status == CallStatus.endedCallBySelf.rawValue || status == CallStatus.callFailed.rawValue || status == CallStatus.endedCallByOther.rawValue {
            callView.leave()
            pushBackToTelecomViewController()
        }
    }
    
    func callNotFound(callId: Int) {
        callView.leave()
        pushBackToTelecomViewController()
    }
    
    func onGoingCallNotFound() {
        callView.leave()
        pushBackToTelecomViewController()
    }
    
    func pushBackToTelecomViewController() {
        if let viewControllers = self.navigationController?.viewControllers, viewControllers.count > 2 {
//            for controller in viewControllers {
//                if controller.isKind(of: TelecomViewController.self) {
//                    self.navigationController?.popToViewController(controller, animated: true)
//                    break
//                }
//
//            }
            self.navigationController?.popToViewController(viewControllers[viewControllers.count-3], animated: true)// Its because we are open call screen from multiple place
        }
    }
}
