//
//  CallListView.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

protocol CallListView: class  {
    func callListWithSussess(callModel: CallModel)
    func callListWithError(error: String)
}
