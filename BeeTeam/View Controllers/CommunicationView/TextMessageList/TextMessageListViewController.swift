//
//  TextMessageListViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class TextMessageListViewController: UIViewController {

    @IBOutlet var textMessageListTableview: UITableView!
    
    let textMessageListPresenter = TextMessageListPresenter(provider: TextMessageListProvider())
    var textMessageListDataSource = [ChatList]()
    var groupChatListDataSource = [ChatList]()
    
    var notificationSenderId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textMessageListPresenter.attachView(view: self)
        
        textMessageListTableview.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textMessageListPresenter.getChatList(count: 30)
    }
    
    func showChatScreen(id: Int) {
        let indexPersonalChat = textMessageListDataSource.firstIndex { (chat) -> Bool in
            chat.employee?.id == id
        }
        
        guard indexPersonalChat != nil else {
            return
        }
        
        self.notificationSenderId = nil
        
        self.performSegue(withIdentifier: "showChatFromTelecom", sender: IndexPath(row: indexPersonalChat!, section: 0))//0 because always personal chat
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChatFromTelecom" {
            if let vc = segue.destination as? ChatViewController, let indexPath = sender as? IndexPath {
                if indexPath.section == 0 {
                    vc.chatDetail = textMessageListDataSource[indexPath.row]
                } else if indexPath.section == 1 {
                    vc.chatDetail = groupChatListDataSource[indexPath.row]
                }
            }
        }
    }
}


extension TextMessageListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.textMessageListDataSource.count : self.groupChatListDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListTableViewCell") as? ChatListTableViewCell
        
        if indexPath.section == 0 {
            cell?.setData(chat: textMessageListDataSource[indexPath.row])
        } else if indexPath.section == 1 {
            cell?.setGroupData(chat: groupChatListDataSource[indexPath.row])
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showChatFromTelecom", sender: indexPath)
    }
}

extension TextMessageListViewController: TextMessageListView {
    func chatListWithSussess(chatListModel: ChatListModel) {
        self.textMessageListDataSource = chatListModel.chatList ?? []
        self.textMessageListTableview.reloadSections(IndexSet(integer: 0), with: .none)
        
        if let id = notificationSenderId {// If Navigate from notification then this screen display
            self.showChatScreen(id: id)
        }
        
        textMessageListPresenter.getGroupChatList(count: 30)
    }
    
    func chatListWithError(error: String) {
        self.textMessageListDataSource.removeAll()
        self.textMessageListTableview.reloadSections(IndexSet(integer: 0), with: .none)
        textMessageListPresenter.getGroupChatList(count: 30)
//        alertShowWithOK(message: error, viewController: self)
    }
    
    func groupChatListWithSussess(chatListModel: ChatListModel) {
        self.groupChatListDataSource = chatListModel.chatList ?? []
        self.textMessageListTableview.reloadSections(IndexSet(integer: 1), with: .none)
    }
    
    func groupChatListWithError(error: String) {
        self.groupChatListDataSource.removeAll()
        self.textMessageListTableview.reloadSections(IndexSet(integer: 1), with: .none)
    }
}
