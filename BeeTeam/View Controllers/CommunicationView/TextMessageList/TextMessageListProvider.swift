//
//  TextMessageListProvider.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class TextMessageListProvider {
    func getChatList(count: Int, successHandler : @escaping(_ objects: ChatListModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.chatList(count: count), isShowProgress: true)
            .onSuccess { (response: ChatListModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getGroupChatList(count: Int, successHandler : @escaping(_ objects: ChatListModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.groupChatList(count: count), isShowProgress: true)
            .onSuccess { (response: ChatListModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
}
