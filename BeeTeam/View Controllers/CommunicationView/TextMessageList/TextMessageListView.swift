//
//  TextMessageListView.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
//import ObjectMapper

protocol TextMessageListView: class  {
    func chatListWithSussess(chatListModel: ChatListModel)
    func chatListWithError(error: String)
    
    func groupChatListWithSussess(chatListModel: ChatListModel)
    func groupChatListWithError(error: String)
}
