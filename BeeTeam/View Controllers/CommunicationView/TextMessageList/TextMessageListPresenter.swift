//
//  TextMessageListPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class TextMessageListPresenter: NSObject {
    let provider: TextMessageListProvider
    weak private var textMessageListView: TextMessageListView?
    
    init(provider: TextMessageListProvider) {
        self.provider = provider
    }
    
    func attachView(view: TextMessageListView?) {
        guard let view = view else { return }
        textMessageListView = view
    }
    
    func getChatList(count: Int) {
        provider.getChatList(count: count, successHandler: { (response) in
            self.textMessageListView?.chatListWithSussess(chatListModel: response)
        }) { (error) in
            self.textMessageListView?.chatListWithError(error: error)
        }
    }
    
    func getGroupChatList(count: Int) {
        provider.getGroupChatList(count: count, successHandler: { (response) in
            self.textMessageListView?.groupChatListWithSussess(chatListModel: response)
        }) { (error) in
            self.textMessageListView?.groupChatListWithError(error: error)
        }
    }
    
}
