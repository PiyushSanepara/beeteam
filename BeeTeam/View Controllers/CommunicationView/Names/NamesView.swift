//
//  NamesView.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

protocol NamesView: class  {
    func namesWithSussess(peopleListModel: PeopleListModel)
    func namesWithError(error: String)
}
