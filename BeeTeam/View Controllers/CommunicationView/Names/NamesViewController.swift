//
//  NamesViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class NamesViewController: UIViewController {

    let namesPresenter = NamesPresenter(provider: NamesProvider())
    var employeeDataSource = [Employee]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        namesPresenter.attachView(view: self)
        namesPresenter.searchPeople(next: "skip=0&name=")
    }
}

extension NamesViewController: NamesView {
    func namesWithSussess(peopleListModel: PeopleListModel) {
        self.employeeDataSource = peopleListModel.employees ?? []
        
        print(self.employeeDataSource)
    }
    
    func namesWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
}
