//
//  NamesProvider.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class NamesProvider {
    func searchPeople(next: String?, successHandler : @escaping(_ objects: PeopleListModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.peopleSearch(next: next), isShowProgress: true)
            .onSuccess { (response: PeopleListModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
}
