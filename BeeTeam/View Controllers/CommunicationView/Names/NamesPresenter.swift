//
//  NamesPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class NamesPresenter: NSObject {
    let provider: NamesProvider
    weak private var namesView: NamesView?
    
    init(provider: NamesProvider) {
        self.provider = provider
    }
    
    func attachView(view: NamesView?) {
        guard let view = view else { return }
        namesView = view
    }
    
    func searchPeople(next: String?) {
        provider.searchPeople(next: next, successHandler: { (response) in
            self.namesView?.namesWithSussess(peopleListModel: response)
        }) { (error) in
            self.namesView?.namesWithError(error: error)
        }
    }
    
}
