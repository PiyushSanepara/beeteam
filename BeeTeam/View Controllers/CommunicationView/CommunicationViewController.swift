//
//  CommunicationViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class CommunicationViewController: UIViewController {

    @IBOutlet var navigationTitleView: UIView!
    @IBOutlet weak var telecomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configTapGesture()
        self.configNavigationBar()
    }
    
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
        self.setupView()
    }
    
    func configTapGesture() {
        let telecomTapGesture = UITapGestureRecognizer(target: self, action: #selector(telecomAction))
        self.telecomView.isUserInteractionEnabled = true
        self.telecomView.addGestureRecognizer(telecomTapGesture)
    }
    
    @objc func telecomAction() {
        self.performSegue(withIdentifier: "showTelecomFromCommunication", sender: nil)
    }
}

extension CommunicationViewController {
    func setupView() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "sidemenu"), style: .plain, target: self, action: #selector(sideMenuClicked))
//        menuButton.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = menuButton
    }
    
    @objc func sideMenuClicked() {
        presentRightMenuViewController()
    }
}
