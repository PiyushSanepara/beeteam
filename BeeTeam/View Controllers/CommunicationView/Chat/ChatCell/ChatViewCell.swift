//
//  ChatViewCell.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class ChatViewCell: UITableViewCell {

    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackgroundReciver: UIImageView!
    @IBOutlet weak var statusIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setChatData(messageDetail: Message) {
        
        message.text = messageDetail.messageText?.utf8DecodedString()
        
        if self.reuseIdentifier == "SenderCell" {
            if messageDetail.readDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "seen-icon")
            } else if messageDetail.receivedDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "received-icon")
            } else if messageDetail.creationDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "sent-icon")
            } else {
                self.statusIconImageView.image = UIImage(named: "waiting-icon")
            }
        }
    }

}
