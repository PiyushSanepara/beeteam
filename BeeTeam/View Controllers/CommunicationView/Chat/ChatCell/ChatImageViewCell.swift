//
//  ChatImageViewCell.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 16/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class ChatImageViewCell: UITableViewCell {
    
    @IBOutlet weak var chatImage: UIImageView!
//    @IBOutlet weak var viewtrailingConstraint: NSLayoutConstraint!
//    @IBOutlet weak var viewleadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(messageDetail: Message) {
        if let materialData = messageDetail.materials?.first, materialData.type == MaterialType.Image.rawValue, let id = materialData.id, let url = URL(string: UrlConstant.baseURLMaterialChat + "?id=\(id)") {
            self.chatImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_logo_placeholder"))
            self.chatImage.setupImageViewer()
        } else {
            self.chatImage.image = UIImage(named: "ic_logo_placeholder")
        }
        
        if self.reuseIdentifier == "SenderImageViewCell" {
            if messageDetail.readDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "seen-icon")
            } else if messageDetail.receivedDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "received-icon")
            } else if messageDetail.creationDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "sent-icon")
            } else {
                self.statusIconImageView.image = UIImage(named: "waiting-icon")
            }
        }
        
//        let isUserSelf = chatData.senderId == UserDefault.getLoginDetails()?.employee?.id ? true : false
//        viewleadingConstraint.priority = isUserSelf ? .defaultLow : .defaultHigh
//        viewtrailingConstraint.priority = isUserSelf ? .defaultLow : .defaultHigh
    }
    
    func setGroupImage(messageDetail: Message) {
            if let materialData = messageDetail.materials?.first, materialData.type == MaterialType.Image.rawValue, let id = materialData.id, let url = URL(string: UrlConstant.baseURLGroupMaterialChat + "?id=\(id)") {
                self.chatImage.setSDImage(url: url, placeHolderImage: UIImage(named: "ic_logo_placeholder"))
                self.chatImage.setupImageViewer()
            } else {
                self.chatImage.image = UIImage(named: "ic_logo_placeholder")
            }
            
            if self.reuseIdentifier == "SenderImageViewCell" {
                if messageDetail.readDateTime != nil {
                    self.statusIconImageView.image = UIImage(named: "seen-icon")
                } else if messageDetail.receivedDateTime != nil {
                    self.statusIconImageView.image = UIImage(named: "received-icon")
                } else if messageDetail.creationDateTime != nil {
                    self.statusIconImageView.image = UIImage(named: "sent-icon")
                } else {
                    self.statusIconImageView.image = UIImage(named: "waiting-icon")
                }
            }
            
    //        let isUserSelf = chatData.senderId == UserDefault.getLoginDetails()?.employee?.id ? true : false
    //        viewleadingConstraint.priority = isUserSelf ? .defaultLow : .defaultHigh
    //        viewtrailingConstraint.priority = isUserSelf ? .defaultLow : .defaultHigh
        }

}
