//
//  ChatAudioCell.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 16/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

protocol ChatAudioCellDelegate {
    func playSenderAction(cell: ChatAudioCell)
    func playReceiverAction(cell: ChatAudioCell)
}

class ChatAudioCell: UITableViewCell {
    
    @IBOutlet weak var statusIconImageView: UIImageView!
    @IBOutlet weak var playSenderButton: UIButton!
    @IBOutlet weak var playReceiverButton: UIButton!
    
    var delegate: ChatAudioCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func playSenderAction(_ sender: Any) {
        if let del = self.delegate {
            del.playSenderAction(cell: self)
        }
    }
    
    @IBAction func playReceiverAction(_ sender: Any) {
        if let del = self.delegate {
            del.playReceiverAction(cell: self)
        }
    }
    
    func setImage(messageDetail: Message) {        
        if self.reuseIdentifier == "SenderAudioCell" {
            self.playSenderButton.isSelected = messageDetail.isPlaying
            if messageDetail.readDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "seen-icon")
            } else if messageDetail.receivedDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "received-icon")
            } else if messageDetail.creationDateTime != nil {
                self.statusIconImageView.image = UIImage(named: "sent-icon")
            } else {
                self.statusIconImageView.image = UIImage(named: "waiting-icon")
            }
        } else {
            self.playReceiverButton.isSelected = messageDetail.isPlaying
        }
    }
    
//    func setGroupImage(messageDetail: Message) {
//        if self.reuseIdentifier == "SenderAudioCell" {
//            if messageDetail.readDateTime != nil {
//                self.statusIconImageView.image = UIImage(named: "seen-icon")
//            } else if messageDetail.receivedDateTime != nil {
//                self.statusIconImageView.image = UIImage(named: "received-icon")
//            } else if messageDetail.creationDateTime != nil {
//                self.statusIconImageView.image = UIImage(named: "sent-icon")
//            } else {
//                self.statusIconImageView.image = UIImage(named: "waiting-icon")
//            }
//        }
//    }

}
