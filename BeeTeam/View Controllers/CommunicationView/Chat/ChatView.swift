//
//  ChatView.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

protocol ChatView: class  {
    func chatWithSussess(messageModel: MessageModel)
    func chatWithError(error: String)
}
