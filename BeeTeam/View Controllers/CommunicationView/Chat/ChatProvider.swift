//
//  ChatProvider.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class ChatProvider {
    func getChat(count: Int, secondParty: Int, beforeTime: String?, successHandler : @escaping(_ objects: MessageModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.chat(count: count, secondParty: secondParty, beforeTime: beforeTime), isShowProgress: true)
            .onSuccess { (response: MessageModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
    func getGroupChat(count: Int, groupId: Int, beforeTime: String?, successHandler : @escaping(_ objects: MessageModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.groupChat(count: count, groupId: groupId, beforeTime: beforeTime), isShowProgress: true)
            .onSuccess { (response: MessageModel) in
                successHandler(response)
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
    
}
