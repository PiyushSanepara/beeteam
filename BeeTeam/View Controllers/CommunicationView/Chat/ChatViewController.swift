//
//  ChatViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import AVFoundation
import YPImagePicker
import AVKit
import IQKeyboardManagerSwift
import iRecordView
class ChatViewController: UIViewController {
    
    @IBOutlet var navigationTitleView: UIView!
    @IBOutlet weak var tblChatList: UITableView!
    @IBOutlet weak var profileImageReceiver: UIImageView!
    @IBOutlet weak var lblReceiverName: UILabel!
    @IBOutlet weak var lblReceiverStatus: UILabel!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var btnAudio: RecordButton!
    
    var player: AVPlayer?
    
    let chatPresenter = ChatPresenter(provider: ChatProvider())
    var chatDetail: ChatList?
    var chatDataSource = [Message]()
    let receiverCellId = "ReceiverCell"
    let senderCellId = "SenderCell"
    let receiverImageCellId = "ReceiverImageViewCell"
    let senderImageCellId = "SenderImageViewCell"
    let receiverAudioCellId = "ReceiverAudioCell"
    let senderAudioCellId = "SenderAudioCell"
    
    var isGettingData = false
    var isGettingAllData = false
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    let recordView = RecordView()
    var isAudioCancel = false
    var audioPlayedIndex: IndexPath?
//    var isAudioCall: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configNavigationBar()
        chatPresenter.attachView(view: self)
        self.setBackgroundImage()
        self.setBorderToTextView()
        tblChatList.transform = CGAffineTransform (scaleX: 1,y: -1);
        tblChatList.estimatedRowHeight = 60
        tblChatList.rowHeight = UITableView.automaticDimension
        tblChatList.tableFooterView = UIView()
        tblChatList.register(UINib(nibName: receiverCellId, bundle: nil), forCellReuseIdentifier: receiverCellId)
        tblChatList.register(UINib(nibName: senderCellId, bundle: nil), forCellReuseIdentifier: senderCellId)
        tblChatList.register(UINib(nibName: receiverImageCellId, bundle: nil), forCellReuseIdentifier: receiverImageCellId)
        tblChatList.register(UINib(nibName: senderImageCellId, bundle: nil), forCellReuseIdentifier: senderImageCellId)
        tblChatList.register(UINib(nibName: receiverAudioCellId, bundle: nil), forCellReuseIdentifier: receiverAudioCellId)
        tblChatList.register(UINib(nibName: senderAudioCellId, bundle: nil), forCellReuseIdentifier: senderAudioCellId)
        self.configData()
//        SignalRClientManager.shared.initialEvents(isGroupChat: self.isGroupChat()) { (_ isConnected) in
//            print("status for callback", isConnected)
//            if isConnected {
                self.isGroupChat() ? self.getGroupChatData() : self.getChatData()
//            }
//        }
        loadRecordingSession()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !txtMsg.isFirstResponder {
            txtMsg.text = self.getTextPlaceholder()
        }
        self.updateSendButtonState(self.sendButtonStatus(text: self.txtMsg.text ?? ""))
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        SignalRClientManager.shared.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if txtMsg.isFirstResponder {
            txtMsg.resignFirstResponder()
        }
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        
        if self.isMovingFromParent {//Dont need to stop connection if presenting some screen just stop if it is back.
            //SignalRClientManager.shared.connection.stop()
            SignalRClientManager.shared.delegate = nil
        }
    }
    
    func configNavigationBar() {
        self.navigationItem.titleView = self.navigationTitleView
    }
    
    func configData() {
        if isGroupChat() {
            self.lblReceiverName.text = chatDetail?.groupHeader
            if let id = chatDetail?.groupId {
                if let url = URL(string: UrlConstant.baseURLGroupPicture + "?id=\(id)") {
                    DispatchQueue.main.async {
                        self.profileImageReceiver.setSDImage(url: url, placeHolderImage: UIImage(named: "chatMultiplePlaceholder"))
                    }
                } else {
                    self.profileImageReceiver.image = UIImage(named: "chatMultiplePlaceholder")
                }
            }
        } else {
            self.lblReceiverName.text = chatDetail?.employee?.arFullName
            if let id = chatDetail?.employee?.id {
                if let url = URL(string: UrlConstant.baseURLEmployeePicture + "?id=\(id)") {
                    DispatchQueue.main.async {
                        self.profileImageReceiver.setSDImage(url: url, placeHolderImage: UIImage(named: "chatSinglePlaceholder"))
                    }
                } else {
                    self.profileImageReceiver.image = UIImage(named: "chatSinglePlaceholder")
                }
            }
        }
    }
    
    func setBackgroundImage(){
        let background = UIImage(named: "chat-bg")
        
        var imageView : UIImageView!
        imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
    }
    
    func setBorderToTextView() {
        self.viewText.layer.borderColor = #colorLiteral(red: 0.1920000017, green: 0.05900000036, blue: 0.0549999997, alpha: 1)
        self.viewText.layer.borderWidth = 1.0
        
    }
    
    func isGroupChat() -> Bool {
        return (chatDetail?.groupId != nil) ? true : false
    }
    
    func getChatData() {
        if !isGettingData, !isGettingAllData {
            self.isGettingData = true
            if let id = chatDetail?.employee?.id {
                if self.chatDataSource.count > 0, let beforeTime = self.chatDataSource.last?.creationDateTime {
                    chatPresenter.getChat(count: 20, secondParty: id, beforeTime: beforeTime)
                } else {
                    chatPresenter.getChat(count: 20, secondParty: id, beforeTime: nil)
                }
            }
        }
    }
    
    func getGroupChatData() {
        if !isGettingData, !isGettingAllData {
            self.isGettingData = true
            if let id = chatDetail?.groupId {
                if self.chatDataSource.count > 0, let beforeTime = self.chatDataSource.last?.creationDateTime {
                    chatPresenter.getGroupChat(count: 20, groupId: id, beforeTime: beforeTime)
                } else {
                    chatPresenter.getGroupChat(count: 20, groupId: id, beforeTime: nil)
                }
            }
        }
    }
    
    func sendMessage(materialId: Int?) {
        var materialArray = [Int]()
        if let id = materialId {
            materialArray.append(id)
        }
        if isGroupChat() {
            SignalRClientManager.shared.sendGroupMessage(messageData: [chatDetail?.groupId ?? 0, (materialArray.count > 0) ? "" : txtMsg.text.utf8EncodedString(), Int(Int32.random(in: 0 ... Int32.max)), materialArray])
        } else {
            SignalRClientManager.shared.sendMessage(messageData: [chatDetail?.employee?.id ?? 0, (materialArray.count > 0) ? "" : txtMsg.text.utf8EncodedString(), Int(Int32.random(in: 0 ... Int32.max)), materialArray])
        }
    }
    
    //    Mark: Actions
    @IBAction func btnSendMessageTapped(_ sender: UIButton) {
        self.updateSendButtonState(false)
        self.sendMessage(materialId: nil)//nil because its without material
        self.txtMsg.text = nil
    }
    
    // MARK: - Enable and Disable Button State
    func updateSendButtonState(_ isDisbale: Bool) {
        if !isDisbale {
            self.btnAudio.isHidden = false
            self.btnSend.isHidden = true
        }else {
            self.btnAudio.isHidden = true
            self.btnSend.isHidden = false
        }
    }
    
    func getTextPlaceholder() -> String {
        return "اكتب رسالة هنا"
    }
    
    func isShowPlaceHolder(text: String?) -> Bool {
        if text == "" || text == nil || text == getTextPlaceholder() {
            return true
        }
        return false
    }
    
    func sendButtonStatus(text: String?) -> Bool {
        if text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count ?? 0 > 0 {
            if isShowPlaceHolder(text: text) {
                return false
            }
            return true
        }
        return false
    }
    
    @IBAction func cameraAction(_ sender: Any) {
        self.showPicker()
    }
    
    @IBAction func attachmentAction(_ sender: Any) {
        
    }
    
    @IBAction func btnAudioCallTapped(_ sender: UIButton) {
//        isAudioCall = true
        SignalRClientManager.shared.isAudioCall = true
        SignalRClientManager.shared.initiateCallWithOption(callOption: [chatDetail?.employee?.id ?? 0, false])
    }
    
    @IBAction func btnVideoCallTapped(_ sender: UIButton) {
//        isAudioCall = false
        SignalRClientManager.shared.isAudioCall = false
        SignalRClientManager.shared.initiateCallWithOption(callOption: [chatDetail?.employee?.id ?? 0, true])
    }
}

extension ChatViewController: SignalRManagerDelegate {
    func newMessage(message: Message?, isSent: Bool) {
        if let msg = message {
            self.chatDataSource.insert(msg, at: 0)
            if !isSent {
                SignalRClientManager.shared.messageLgRd(messageData: [message?.id ?? 0])
            }
            self.tblChatList.reloadData()
        }
    }
    
    func logSentReceipt(msgID: Int?, receivedDateTime: String?) {
        if let message = self.chatDataSource.first(where: {$0.id == msgID}) {
            message.receivedDateTime = receivedDateTime
            //            self.chatDataSource.insert(message, at: 0)
            self.tblChatList.reloadData()
        }
    }
    
    func logSentRead(msgID: Int?, readDateTime: String?) {
        if let message = self.chatDataSource.first(where: {$0.id == msgID}) {
            message.readDateTime = readDateTime
            self.tblChatList.reloadData()
        }
    }
    
    func updateReadReceiptStatus(messages: [Message]) {
        if isGroupChat() {
            for msg in messages where msg.readDateTime == nil {
                if msg.receivedDateTime == nil {
                    SignalRClientManager.shared.groupMessageLgRcpt(messageData: [msg.id ?? 0])
                }
                SignalRClientManager.shared.groupMessageLgRd(messageData: [msg.id ?? 0])
            }
        } else {
            for msg in messages where msg.readDateTime == nil {
                if msg.receivedDateTime == nil {
                    SignalRClientManager.shared.messageLgRcpt(messageData: [msg.id ?? 0])
                }
                SignalRClientManager.shared.messageLgRd(messageData: [msg.id ?? 0])
            }
        }
    }
    
//    func callInitiateWith(callId: Int, caller: Employee?, receiver: Employee?) {
//        if let vc = CommonMethods.viewController(name: .communication, identifier: "CallerRingViewController") as? CallerRingViewController {
//            vc.callId = callId
//            vc.receiverData = receiver
//            vc.isAudioCall = isAudioCall
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
}

extension ChatViewController: ChatView {
    func chatWithSussess(messageModel: MessageModel) {
        if messageModel.messageList?.count == 0 {
            self.isGettingAllData = true
            return
        }
        if (self.chatDataSource.count == 0) {
            self.chatDataSource = messageModel.messageList ?? []
        } else {
            self.chatDataSource.append(contentsOf: messageModel.messageList ?? [])
        }
        
        self.updateReadReceiptStatus(messages: messageModel.messageList ?? [])
        
        self.isGettingData = false
        
        DispatchQueue.main.async {
            self.tblChatList.reloadData()
        }
    }
    
    func chatWithError(error: String) {
        self.chatDataSource.removeAll()
        alertShowWithOK(message: error, viewController: self)
    }
}

extension ChatViewController: UITableViewDataSource, UITableViewDelegate, ChatAudioCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatData = chatDataSource[indexPath.row]
        
        if chatDataSource.count > 0, indexPath.row == chatDataSource.count - 3 {
            isGroupChat() ? self.getGroupChatData() : self.getChatData()
        }
        
        if isGroupChat() {
            if chatData.materials?.count ?? 0 > 0 {
                if chatData.materials?[0].type == MaterialType.audio.rawValue {
                    let audioCell = tableView.dequeueReusableCell(withIdentifier: !chatData.me ? receiverAudioCellId : senderAudioCellId, for: indexPath) as? ChatAudioCell
                    audioCell?.delegate = self
                    audioCell?.setImage(messageDetail: chatData)
                    audioCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                    return audioCell ?? ChatAudioCell()
                } else if chatData.materials?[0].type == MaterialType.Image.rawValue || chatData.materials?[0].type == MaterialType.Video.rawValue {
                    let chatImageCell = tableView.dequeueReusableCell(withIdentifier: !chatData.me ? receiverImageCellId : senderImageCellId, for: indexPath) as? ChatImageViewCell
                    chatImageCell?.setGroupImage(messageDetail: chatData)
                    chatImageCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                    return chatImageCell ?? ChatImageViewCell()
                }
                return UITableViewCell()
            } else {
                let chatCell = tableView.dequeueReusableCell(withIdentifier: !chatData.me ? receiverCellId : senderCellId, for: indexPath) as? ChatViewCell
                chatCell?.setChatData(messageDetail: chatData)
                chatCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                return chatCell ?? ChatViewCell()
            }
        } else {
            if chatData.materials?.count ?? 0 > 0 {
                if chatData.materials?[0].type == MaterialType.audio.rawValue {
                    let audioCell = tableView.dequeueReusableCell(withIdentifier: !chatData.me ? receiverAudioCellId : senderAudioCellId, for: indexPath) as? ChatAudioCell
                    audioCell?.delegate = self
                    audioCell?.setImage(messageDetail: chatData)
                    audioCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                    return audioCell ?? ChatAudioCell()
                } else if chatData.materials?[0].type == MaterialType.Image.rawValue || chatData.materials?[0].type == MaterialType.Video.rawValue {
                    let chatImageCell = tableView.dequeueReusableCell(withIdentifier: chatData.senderId != UserDefault.getLoginDetails()?.employee?.id ? receiverImageCellId : senderImageCellId, for: indexPath) as? ChatImageViewCell
                    chatImageCell?.setImage(messageDetail: chatData)
                    chatImageCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                    return chatImageCell ?? ChatImageViewCell()
                }
                return UITableViewCell()
            } else {
                let chatCell = tableView.dequeueReusableCell(withIdentifier: chatData.senderId != UserDefault.getLoginDetails()?.employee?.id ? receiverCellId : senderCellId, for: indexPath) as? ChatViewCell
                chatCell?.setChatData(messageDetail: chatData)
                chatCell?.contentView.transform = CGAffineTransform (scaleX: 1,y: -1);
                return chatCell ?? ChatViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func playSenderAction(cell: ChatAudioCell) {
        guard let indexPath = self.tblChatList.indexPath(for: cell) else { return }
//        print(indexPath)
        
        let headers: [String: String] = [HeaderConstant.authorization: UserDefault.getToken()]
        if let materialArr = chatDataSource[indexPath.row].materials, materialArr.count > 0 {
            if materialArr[0].type == MaterialType.audio.rawValue, let id = materialArr[0].id {
                if let url = URL(string: (isGroupChat() ? UrlConstant.baseURLGroupMaterialChat : UrlConstant.baseURLMaterialChat) + "?id=\(id)") {
                    if (player == nil) || (self.audioPlayedIndex != indexPath) {
                        let audioSession = AVAudioSession.sharedInstance()
                        do {
                            try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                        }catch let error {
                            print("error while change audio session", error.localizedDescription)
                        }
                        
                        let asset = AVURLAsset(url: url, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
                        let playerItem = AVPlayerItem(asset: asset)
                        player = AVPlayer(playerItem: playerItem)
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
                        
                        if let previousIndex = self.audioPlayedIndex {
                            chatDataSource[previousIndex.row].isPlaying = false
                            self.tblChatList.reloadRows(at: [previousIndex], with: .none)
                        }
                        
                        chatDataSource[indexPath.row].isPlaying = true
                        self.tblChatList.reloadRows(at: [indexPath], with: .none)
                        self.audioPlayedIndex = indexPath
                        player?.play()
                    } else {
                        chatDataSource[indexPath.row].isPlaying = false
                        self.tblChatList.reloadRows(at: [indexPath], with: .none)
                        self.audioPlayedIndex = nil
                        player?.pause()
                        player = nil
                    }
                    
                }
            }
        }
        
    }
    
    func playReceiverAction(cell: ChatAudioCell) {
        guard let indexPath = self.tblChatList.indexPath(for: cell) else { return }
        
        let headers: [String: String] = [HeaderConstant.authorization: UserDefault.getToken()]
        if let materialArr = chatDataSource[indexPath.row].materials, materialArr.count > 0 {
            if materialArr[0].type == MaterialType.audio.rawValue, let id = materialArr[0].id {
                if let url = URL(string: (isGroupChat() ? UrlConstant.baseURLGroupMaterialChat : UrlConstant.baseURLMaterialChat) + "?id=\(id)") {
                    if (player == nil) || (self.audioPlayedIndex != indexPath) {
                        let asset = AVURLAsset(url: url, options: ["AVURLAssetHTTPHeaderFieldsKey": headers])
                        let playerItem = AVPlayerItem(asset: asset)
                        player = AVPlayer(playerItem: playerItem)
//                        cell.playReceiverButton.isSelected = true
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(notification:)), name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
                        
                        if let previousIndex = self.audioPlayedIndex {
                            chatDataSource[previousIndex.row].isPlaying = false
                            self.tblChatList.reloadRows(at: [previousIndex], with: .none)
                        }
                        
                        chatDataSource[indexPath.row].isPlaying = true
                        self.tblChatList.reloadRows(at: [indexPath], with: .none)
                        self.audioPlayedIndex = indexPath
                        player?.play()
                    } else {
//                        cell.playReceiverButton.isSelected = false
                        chatDataSource[indexPath.row].isPlaying = false
                        self.tblChatList.reloadRows(at: [indexPath], with: .none)
                        self.audioPlayedIndex = nil
                        player?.pause()
                        player = nil
                    }
                    
                }
            }
        }
    }
    
    @objc func playerDidFinishPlaying(notification: Notification) {
        print(notification)
        
        if let indexPath = self.audioPlayedIndex {
            chatDataSource[indexPath.row].isPlaying = false
            self.tblChatList.reloadRows(at: [indexPath], with: .none)
        }
        
//        if let item = notification.object as? AVPlayerItem, let asset = item.asset as? AVURLAsset, let urlString = asset.url.absoluteString as? String {
//            print(urlString)
//            NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: nil)
//
//
//        }
        
    }
}

extension ChatViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if chatDataSource.count > 0 {
            self.tblChatList.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.updateSendButtonState(self.sendButtonStatus(text: textView.text))
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        DispatchQueue.main.async {
            if self.isShowPlaceHolder(text: textView.text) {
                self.txtMsg.text = nil
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let isShowPlaceHolder = self.isShowPlaceHolder(text: textView.text)
        if isShowPlaceHolder {
            txtMsg.text = self.getTextPlaceholder()
        }
        updateSendButtonState(self.sendButtonStatus(text: textView.text))
    }
}

extension ChatViewController {
    @objc func showPicker() {
        
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photoAndVideo//.photoAndVideo
        
        config.shouldSaveNewPicturesToAlbum = false
        
        config.video.compression = AVAssetExportPresetLowQuality
        
        config.startOnScreen = .library
        
        config.screens = [.library, .photo, .video]//[.library, .photo, .video]
        
        config.video.libraryTimeLimit = 500.0
        
        config.showsCrop = .none
        
        config.wordings.libraryTitle = "Gallery"
        
        config.hidesStatusBar = false
        
        config.hidesBottomBar = false
        
        config.maxCameraZoomFactor = 2.0
        
        config.library.maxNumberOfItems = 1
        config.gallery.hidesRemoveButton = false
        
        config.library.preselectedItems = nil //selectedItems
        
        let picker = YPImagePicker(configuration: config)
//        picker.supportedInterfaceOrientations = UIInterfaceOrientationMask.landscapeRight
        
        /* Multiple media implementation */
        picker.didFinishPicking { [unowned picker] items, cancelled in
            
            if cancelled {
                print("Picker was canceled")
                picker.dismiss(animated: true, completion: nil)
                return
            }
            
            _ = items.map { print("🧀 \($0)") }
            
            //            self.selectedItems = items
            if let firstItem = items.first {
                switch firstItem {
                case .photo(let photo):
                    //                    self.selectedImageV.image = photo.image
                    
                    let jpgImageData = photo.image.jpegData(compressionQuality: 0.7)
                    
                    UtilCommonWebService.socialUpload(qqfile: jpgImageData, qqfilename: "\((photo.asset?.value(forKey: "filename") as? String)?.removeExtension() ?? "defalutImageName").jpeg", successHandler: { (materialId) in
                        
                        //                        print(postId)
                        self.sendMessage(materialId: materialId)
                        
                        picker.dismiss(animated: true, completion: nil)
                    }) { (error) in
                        alertShowWithOK(message: error, viewController: self)
                        picker.dismiss(animated: true, completion: nil)
                    }
                case .video(let video):
                    do {
                        let videoData = try Data(contentsOf: video.url, options: .mappedIfSafe)
                        UtilCommonWebService.socialUpload(qqfile: videoData, qqfilename: video.asset?.value(forKey: "filename") as? String, successHandler: { (materialId) in
                            self.sendMessage(materialId: materialId)
                            picker.dismiss(animated: true, completion: nil)
                        }) { (error) in
                            alertShowWithOK("BeeTeam", message: error, viewController: self)
                            picker.dismiss(animated: true, completion: nil)
                        }
                    } catch  {
                    }
                }
            }
        }
        
        present(picker, animated: true, completion: nil)
    }
    
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}

extension ChatViewController: RecordViewDelegate {
    func onStart() {
        self.resetBottomViewOpacity(isHidden: true, duration: 0.2)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.startRecording()
        }
    }
    
    func onCancel() {
        isAudioCancel = true
    }
    
    func onFinished(duration: CGFloat) {
        self.resetBottomViewOpacity(isHidden: false, duration: 0.2)
        finishRecording(success: true)
    }
    
    func onAnimationEnd() {
        self.resetBottomViewOpacity(isHidden: false, duration: 0.2)
        finishRecording(success: true)
    }
    
    @objc func resetBottomViewOpacity(isHidden: Bool, duration: TimeInterval) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.viewBottom.isHidden = isHidden
        }
    }
}

extension ChatViewController: AVAudioRecorderDelegate {
    func loadRecordingSession() {
        recordView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(recordView)
        
        recordView.trailingAnchor.constraint(equalTo: btnSend.leadingAnchor, constant: -20).isActive = true
        recordView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        recordView.bottomAnchor.constraint(equalTo: btnSend.bottomAnchor).isActive = true
        btnAudio.recordView = recordView
        recordView.delegate = self

        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if !allowed {
                        let message = "BeeTeam needs Microphone access in order to send audio. Please go to “Settings” and enable Microphone access for BeeTeam."
                        self.displayAlert(with: "Unable to Access Microphone", message: message, buttonTitles: ["Settings", "Cancel"], alertActionStyles: [.default, .cancel]) { (title) in
                            if title == "Settings" {
                                self.opneApplicationSettings {  }
                            }
                        }
                    }
                }
            }
        } catch {
            // failed to record!
        }
    }
    
    func startRecording() {
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.aac")
        
        let settings = [
            AVFormatIDKey: kAudioFormatMPEG4AAC,
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ] as [String : Any]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
        } catch let error {
            print("getting error while recording", error.localizedDescription)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        if audioRecorder != nil {
            audioRecorder.stop()
            audioRecorder = nil
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        let asset = AVURLAsset(url: recorder.url, options: nil)
        let audioDuration = asset.duration
        let audioDurationSeconds = Int(CMTimeGetSeconds(audioDuration))
        if flag && !isAudioCancel && audioDurationSeconds > 0 {//JV I Changed this it works fine please confirm there is not any problen.
            do {
                let videoData = try Data(contentsOf: recorder.url, options: .mappedIfSafe)
                UtilCommonWebService.socialUpload(qqfile: videoData, qqfilename: "recording\(Int(Int32.random(in: 0 ... Int32.max))).aac", successHandler: { (materialId) in
                    self.sendMessage(materialId: materialId)
                }) { (error) in
                    alertShowWithOK("BeeTeam", message: error, viewController: self)
                }
            } catch  {
            }
        }else {
            isAudioCancel = false
        }
    }
}
