//
//  ChatPresenter.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 14/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class ChatPresenter: NSObject {
    let provider: ChatProvider
    weak private var chatView: ChatView?
    
    init(provider: ChatProvider) {
        self.provider = provider
    }
    
    func attachView(view: ChatView?) {
        guard let view = view else { return }
        chatView = view
    }
    
    func getChat(count: Int, secondParty: Int, beforeTime: String?) {
        provider.getChat(count: count, secondParty: secondParty, beforeTime: beforeTime, successHandler: { (response) in
            self.chatView?.chatWithSussess(messageModel: response)
        }) { (error) in
            self.chatView?.chatWithError(error: error)
        }
    }
    
    func getGroupChat(count: Int, groupId: Int, beforeTime: String?) {
        provider.getGroupChat(count: count, groupId: groupId, beforeTime: beforeTime, successHandler: { (response) in
            self.chatView?.chatWithSussess(messageModel: response)
        }) { (error) in
            self.chatView?.chatWithError(error: error)
        }
    }
    
}
