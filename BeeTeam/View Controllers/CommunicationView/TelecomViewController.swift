//
//  TelecomViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import CarbonKit

class TelecomViewController: UIViewController {
    
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var currentIndex = 1
    
    @IBOutlet var navigationTitleView: UIView!
    var notificationSenderId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configNavigationBar()
        
        self.configSegmentButtons()
    }
        
    func configNavigationBar() {
        self.title = " "
        self.navigationItem.titleView = self.navigationTitleView
    }

}

extension TelecomViewController: CarbonTabSwipeNavigationDelegate {
  func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
    
    let strbrd = UIStoryboard(name: "Communication", bundle: nil)
    switch index {
    case 0:
        let vc = strbrd.instantiateViewController(withIdentifier: "CallListViewController") as! CallListViewController
            
        return vc
      
    case 1:
      let vc = strbrd.instantiateViewController(withIdentifier: "TextMessageListViewController") as! TextMessageListViewController
      vc.notificationSenderId = self.notificationSenderId
      return vc
        
    case 2:
        let vc = strbrd.instantiateViewController(withIdentifier: "NamesViewController") as! NamesViewController
        
        return vc
              
    default:
      
      return UIViewController()
    }
  }
  
  func configSegmentButtons() {
    let items = ["مكالمات", "رسائل نصية", "الاسماء"]
    
    carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
    carbonTabSwipeNavigation.currentTabIndex = UInt(currentIndex)
    carbonTabSwipeNavigation.insert(intoRootViewController: self)
    carbonTabSwipeNavigation.toolbar.isTranslucent = false
//    carbonTabSwipeNavigation.toolbar.backgroundColor = ConstantColor.Color_Orange
    carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = #colorLiteral(red: 0.8705882353, green: 0.7803921569, blue: 0.7411764706, alpha: 1)
    carbonTabSwipeNavigation.setIndicatorColor(#colorLiteral(red: 0.1921568627, green: 0.05882352941, blue: 0.05490196078, alpha: 1))
    carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0.1921568627, green: 0.05882352941, blue: 0.05490196078, alpha: 1), font: ConstantFont.Font_TitleBold)
    carbonTabSwipeNavigation.setNormalColor(#colorLiteral(red: 0.1921568627, green: 0.05882352941, blue: 0.05490196078, alpha: 1).withAlphaComponent(0.6))
//    carbonTabSwipeNavigation.setTabExtraWidth(50)
    carbonTabSwipeNavigation.setTabBarHeight(50)
//    carbonTabSwipeNavigation.pagesScrollView?.isScrollEnabled = false
    carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(self.view.frame.width / 3, forSegmentAt: 0)
    carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(self.view.frame.width / 3, forSegmentAt: 1)
    carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(self.view.frame.width / 3, forSegmentAt: 2)
  }
}
