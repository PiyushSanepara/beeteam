//
//  NavigationController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 11/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    var sideMenu: SSASideMenu = SSASideMenu()

    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenu.panGestureEnabled = true
        
        self.navigationBar.barTintColor = ConstantColor.Color_Black
        self.navigationBar.tintColor = ConstantColor.Color_Yellow
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: ConstantFont.Font_Title]
    }
}
