
import UIKit

class SideMenuDashboardViewController: UIViewController {
    
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    var menuArray: [String] = ["الرئيسية","صفحتي","تواصل","الحضور والمغادرة","المهام","الاجندة","نماذج","عروض الموظفين", "تسجيل خروج"]
    
    var menuIconArray: [String] = ["home-icon-list","profile-icon","communication-icon-list","Access-icon-list","task-icon-list","calendar-icon","form-icon-list","offers-icon","logout-icon-list"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        sideMenuTableView.backgroundColor = UIColor.white
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sideMenuTableView.reloadData()
    }
    
    func closeMenu() {
        self.sideMenuViewController?.hideMenuViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SideMenuDashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCellTableViewCell") as! MenuCellTableViewCell
        menuCell.lblMenuTitle.text = self.menuArray[indexPath.row]
        menuCell.menuImage.image = UIImage(named: self.menuIconArray[indexPath.row])
        return menuCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.row == 0) {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let navigationController: NavigationController = NavigationController(rootViewController: homeViewController!)
            sideMenuViewController?.contentViewController = navigationController
        }
        else if (indexPath.row == 1) {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            let navigationController: NavigationController = NavigationController(rootViewController: profileViewController!)
            sideMenuViewController?.contentViewController = navigationController
        }
        else if (indexPath.row == 2) {
            let storyboard = UIStoryboard(name: "Communication", bundle: nil)
            let communicationViewController = storyboard.instantiateViewController(withIdentifier: "CommunicationViewController") as? CommunicationViewController
            let navigationController: NavigationController = NavigationController(rootViewController: communicationViewController!)
            sideMenuViewController?.contentViewController = navigationController
        }
//        else if (indexPath.row == 3) {
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let transactionHistoryViewController = storyboard.instantiateViewController(withIdentifier: "TransactionHistoryViewController") as? TransactionHistoryViewController
//            let navigationController: NavigationController = NavigationController(rootViewController: transactionHistoryViewController!)
//            sideMenuViewController?.contentViewController = navigationController
//        }
        else if (indexPath.row == 8) {// Logout
            UserDefault.removeUserDetail()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
            let navigationController: NavigationController = NavigationController(rootViewController: loginViewController!)            
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = navigationController
        }
        if indexPath.row != 8 {
            self.sideMenuViewController?.hideMenuViewController()
        }
    }
}
