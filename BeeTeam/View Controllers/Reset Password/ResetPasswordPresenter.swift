//
//  ResetPasswordPresenter.swift
//  BeeTeam
//

import Foundation


class ResetPasswordPresenter: NSObject {
    let provider: ResetPasswordProvider
    weak private var resetPasswordView: ResetPasswordView?
    
    init(provider: ResetPasswordProvider) {
        self.provider = provider
    }
    
    func attachView(view: ResetPasswordView?) {
        guard let view = view else { return }
        resetPasswordView = view
    }
    
    func resetPasswordUser(email: String) {
        provider.resetPasswordUser(email: email, successHandler: { (response) in
            self.resetPasswordView?.resetPasswordWithSussess(resetPasswordModel: response)
        }) { (error) in
            self.resetPasswordView?.resetPasswordWithError(error: error)
        }
    }
}
