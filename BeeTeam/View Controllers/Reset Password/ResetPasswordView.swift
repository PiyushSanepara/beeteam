//
//  ResetPasswordView.swift
//  BeeTeam
//

import Foundation
import ObjectMapper

protocol ResetPasswordView: class  {
    func resetPasswordWithSussess(resetPasswordModel: LoginModel)
    func resetPasswordWithError(error: String)
}
