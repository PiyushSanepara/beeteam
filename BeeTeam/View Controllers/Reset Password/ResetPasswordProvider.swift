//
//  ResetPasswordProvider.swift
//  BeeTeam
//

import Foundation

class ResetPasswordProvider {
    func resetPasswordUser(email: String, successHandler : @escaping(_ objects: LoginModel) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
        
        NetworkManager.makeRequest(HttpRouter.forgotPassword(email: email), isShowProgress: true)
            .onSuccess { (response: LoginModel) in
            if response.success ?? false {
                successHandler(response)
            } else {
                errorHandler(response.errorMessage ?? "Wrong Credentials")
            }
        }
        .onFailure { error in
            print(error.localizedDescription)
            let errorHandle = ErrorHandler(error: error)
            errorHandler(errorHandle.0)
        }.onComplete { _ in
        }
    }
}
