//
//  ResetPasswordViewController.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 09/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextfield: TextField!
    
    let resetPasswordPresenter = ResetPasswordPresenter(provider: ResetPasswordProvider())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resetPasswordPresenter.attachView(view: self)
    }
    
    func isValidate() -> Bool {
        if self.emailTextfield.text == "" {
            alertShowWithOK("Reset Password Fail", message: AlertConstant.enterEmail, viewController: self)
            return false
        }
        
        if !(self.emailTextfield.text?.isValidEmail() ?? false) {
            alertShowWithOK("Reset Password Fail", message: AlertConstant.invalidEmail, viewController: self)
            return false
        }
        
        return true
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if isValidate() {
            resetPasswordPresenter.resetPasswordUser(email: self.emailTextfield.text ?? "")
        }
   }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)        
    }
    
}

extension ResetPasswordViewController: ResetPasswordView {
    func resetPasswordWithSussess(resetPasswordModel: LoginModel) {
        if resetPasswordModel.success ?? false {
            
//            print(resetPasswordModel.authentication)
//            authantication = loginModel.authentication ?? ""
//            self.openDashboard()
        }
    }
    
    func resetPasswordWithError(error: String) {
        alertShowWithOK(message: error, viewController: self)
    }
}
