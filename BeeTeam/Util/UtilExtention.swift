import UIKit
import SDWebImage

extension UIView {
    func addShadow(){
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
}

extension UITextField {
    func addDropDownIcon() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 28, height: 12))
        let imgView = UIImageView(frame: CGRect(x: 0.0, y: 0, width: 12, height: 12))
        imgView.image = UIImage(named: "ic_dropdown")
        imgView.contentMode = .center
//        imgView.tintColor = color
        view.addSubview(imgView)
        self.rightView = view
        self.rightView?.isUserInteractionEnabled = false
        rightViewMode = .always
        self.tintColor = UIColor.clear
    }
}

extension UIImageView {
    func setSDImage(url: URL, placeHolderImage: UIImage?, completion: (() -> Void)? = nil) {
        let imageDownloader = SDWebImageDownloader.shared
        imageDownloader.setValue(UserDefault.getToken(), forHTTPHeaderField: HeaderConstant.authorization)
        imageDownloader.downloadImage(with: url, options: .handleCookies, progress: { (receivedSize, expectedSize, url) in
            DispatchQueue.main.async {
                self.image = placeHolderImage
                completion?()
            }
        }) { (image, data, error, isFinished) in
            DispatchQueue.main.async {
                if error == nil && image != nil {
                    DispatchQueue.main.async {
                        self.image = image
                        completion?()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.image = placeHolderImage
                        completion?()
                    }
                }
            }
        }
    }
}

extension UIImage {
    func compressImage(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func toData() -> Data? {
        guard let imageData = self.pngData() else { return nil }
        return imageData
    }
    
    
}

import ArabicTimeAgo
extension Date {
    func differenceFromToday() -> Int {
        let cal = Calendar.current
        let diff = cal.dateComponents([.day], from: cal.startOfDay(for: self), to: Date()).day ?? 0
        return abs(diff)
    }
    
    //https://gist.github.com/LifetimeCode/c86e7d7187a3d2a955b976322488a973
    func timeAgo() -> String {
        let timeDifference = self.differenceFromToday()
        
        if timeDifference/14 > 1 {
            return self.string(withFormat: "YYYY-MM-dd", identifier: "ar_DZ")
        }
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return year == 1 ? "منذ سنة" : "منذ " + "\(year)" + " " + "سنة"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "منذ شهر" : "منذ " + "\(month)" + " " + "شهر"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "منذ يوم" : "منذ " + "\(day)" + " " + "يوم"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "منذ ساعة" : "منذ " + "\(hour)" + " " + "ساعة"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "منذ دقيقة" : "منذ " + "\(minute)" + " " + "دقيقة"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "منذ ثانية" : "منذ " + "\(second)" + " " + "ثانية"
        } else {
            let df = DateFormatter(); df.dateFormat = "HH:mm"; df.locale = Locale(identifier: "ar")
            return df.string(from: self)
        }
    }
    
//    func timeAgo() -> String {
//        return TimeAgo.calculate(date: self, isAr: true)
//        let timeDifference = self.differenceFromToday()
//        if timeDifference/7 > 1 {
//            return self.string(withFormat: "dd MMM, YYY hh:mma", identifier: "ar_DZ")
//        }else {
//            let dateString = TimeAgo.calculate(date: self, isAr: true)
//            return Date().toLocalTime().daysSince(self) > 7 ? dateString : dateString + ", \(self.string(withFormat: "hh:mma", identifier: "ar_DZ"))"
//        }
//    }
    
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func daysSince(_ date: Date) -> Double {
        return timeIntervalSince(date)/(3600*24)
    }
    
    func string(withFormat format: String = "dd/MM/yyyy hh:mma", identifier: String = "en_US_POSIX") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: identifier)
        dateFormatter.dateFormat = format
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        return dateFormatter.string(from: self)
    }
}

extension String {
    func convertToDateTime(for formatter: String) -> Date? {
        return configDateFormatter(formatter: formatter).date(from: self)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func removeExtension() -> String {
        var components = self.components(separatedBy: ".")
        if components.count > 1 { // If there is a file extension
          components.removeLast()
          return components.joined(separator: ".")
        } else {
          return self
        }
    }

    func utf8DecodedString()-> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    func utf8EncodedString()-> String {
        let messageData = self.data(using: .nonLossyASCII)
        if let message = String(data: messageData!, encoding: .utf8){
            return message
        }
        return ""
    }
}

extension Int {
    func getCallStatus()-> String {
        switch self {
        case 0:
            return "فشل الاتصال"
        case 1:
            return "يرن"
        case 2:
            return "جار الاتصال"
        case 3:
            return "انتهت مهلة الاتصال"
        case 4:
            return "لا إجابة"
        case 5:
            return "المتصل أنهى الرنين"
        case 6:
            return "تم رفض المكالمة"
        case 7:
            return "المحادثة جارية"
        case 8:
            return "المتصل أنهى المكالمة"
        case 9:
            return "المستلم أنهى المكالمة"
        default:
            return "-"
        }
    }
}

extension Int {
    func getCallType()-> String {
        switch self {
        case 1:
            return "مكالمة هاتفية"
        case 2:
            return "مكالمة صوتية"
        case 3:
            return "مكالمة فيديو"
        default:
            return "-"
        }
    }
}

extension UIColor {
   static func hexStringToUIColor (hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if cString.count != 6 {
            return UIColor.gray
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UserDefaults {

    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }

    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

extension UIViewController {
    func displayAlert(with title: String?, message: String?, buttonTitles: [String], alertActionStyles: [UIAlertAction.Style]? = nil, alertAction: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let styles = alertActionStyles ?? []
        for i in 0 ..< buttonTitles.count {
            let style: UIAlertAction.Style = styles.indices.contains(i) ? styles[i] : .default
            let action = UIAlertAction(title: buttonTitles[i], style: style) { (_) in
                alertAction(buttonTitles[i])
            }
            alertController.addAction(action)
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func opneApplicationSettings(completionHandler: @escaping () -> Void) {
        if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                completionHandler()
            })
        }
    }
}
