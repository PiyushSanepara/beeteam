//
//  UtilUserDefault.swift
//
//  Created by Piyush Sanepara on 24/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation

class UserDefault {
    
    static func removeUserDetail() {
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
        UserDefaults.standard.removeObject(forKey: UserDefaultConstant.token)
        UserDefaults.standard.removeObject(forKey: UserDefaultConstant.loginDetails)
        UserDefaults.standard.removeObject(forKey: UserDefaultConstant.password)
        SignalRClientManager.shared.connection.stop()
    }
    
    static func getToken() -> String {
        return UserDefaults.standard.value(forKey: UserDefaultConstant.token) as? String ?? ""
    }

    static func setToken(token : String) {
        UserDefaults.standard.set(token, forKey: UserDefaultConstant.token)
        UserDefaults.standard.synchronize()
    }
    
//    static func getUser() -> String {
//        return UserDefaults.standard.value(forKey: UserDefaultConstant.token) as? String ?? ""
//    }

    static func getLoginDetails() -> LoginModel? {
        UserDefaults.standard.value(LoginModel.self, forKey: UserDefaultConstant.loginDetails)
    }

    static func setLoginDetails(loginDetails : LoginModel) {
        UserDefaults.standard.set(encodable: loginDetails, forKey: UserDefaultConstant.loginDetails)
        UserDefaults.standard.synchronize()
    }
    
    static func getPassword() -> String {
        return UserDefaults.standard.value(forKey: UserDefaultConstant.password) as? String ?? ""
    }

    static func setPassword(passwordString : String) {
        UserDefaults.standard.set(passwordString, forKey: UserDefaultConstant.password)
        UserDefaults.standard.synchronize()
    }
}
