//
//  Util\.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 25/06/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import MBProgressHUD

func showProgressHud(progressText:String) {
    DispatchQueue.main.async {
        if let window = UIApplication.shared.keyWindow {
            let hud = MBProgressHUD.showAdded(to: window, animated: true)
            hud.label.text = progressText
            hud.label.numberOfLines = 0
        }
    }
}

func hideProgressHud() {
    DispatchQueue.main.async {
        if let window = UIApplication.shared.keyWindow {
            MBProgressHUD.hide(for: window, animated: true)
        }
    }
}

func ErrorHandler(error: NetworkError) -> (String, Error) {
    switch error {
    case .unauthorized(let errorJsonString):
        if let errorJsonString = errorJsonString {
            if let errorResponse = ErrorModel(JSONString: errorJsonString) {
                if let errorCodes = errorResponse.errorCodes, errorCodes.count > 0, let errorMessage = errorCodes[0].errordescription {
                    print(error)
                    return (errorMessage, error)
                }
            }
        }
    case .unauthorizedUser(let errorJsonString):
        if let errorJsonString = errorJsonString {
            if let errorResponse = ErrorModel(JSONString: errorJsonString) {
                if let errorCodes = errorResponse.errorCodes, errorCodes.count > 0, let errorMessage = errorCodes[0].errordescription {
                    print(error)
                    return (errorMessage, error)
                }
            }
        }
    case .nonRecoverable(let errorJsonString):
        if let errorJsonString = errorJsonString {
            if let errorResponse = ErrorModel(JSONString: errorJsonString) {
                if let errorCodes = errorResponse.errorCodes, errorCodes.count > 0, let errorMessage = errorCodes[0].errordescription {
                    print(error)
                    return (errorMessage, error)
                }
            }
        }
    case .unprocessableEntity(let errorJsonString):
        if let errorJsonString = errorJsonString {
            if let errorResponse = ErrorModel(JSONString: errorJsonString) {
                if let errorCodes = errorResponse.errorCodes, errorCodes.count > 0, let errorMessage = errorCodes[0].errordescription {
                    print(error)
                    return (errorMessage, error)
                }
            }
        }
    case .noInternet(let errorJsonString):
        return (errorJsonString, error)
    case .requestTimeOut(let errorString):
        return (errorString, error)
    default :
        print(error.localizedDescription)
    }
    return (AlertConstant.somethingWrong, error)
}

func configDateFormatter(formatter: String) -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formatter
    dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
    
    return dateFormatter
}

func getTimeDifference(fromDate: Date, toDate: Date) -> String {

    let dayHourMinuteSecond: Set<Calendar.Component> = [.hour, .minute, .second]
    let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: fromDate, to: toDate)

    let seconds = "\(difference.second ?? 0)s"
    let minutes = "\(difference.minute ?? 0)m" + " " + seconds
    let hours = "\(difference.hour ?? 0)h" + " " + minutes
    let days = "\(difference.day ?? 0)d" + " " + hours

    if let day = difference.day, day          > 0 { return days }
    if let hour = difference.hour, hour       > 0 { return hours }
    if let minute = difference.minute, minute > 0 { return minutes }
    if let second = difference.second, second > 0 { return seconds }
    return "-"
}

struct CommonMethods {
    static func storyBoard(name: StoryBoard) -> UIStoryboard {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    static func viewController(name: StoryBoard, identifier: String) -> UIViewController {
        return storyBoard(name: name).instantiateViewController(withIdentifier: identifier)
    }
    
    static func sendLocalNotification(notificationId: String, titleMessage: String = "You have a new message", bodyMessage: String = "Go to the commnunication center to view it", emp_id: Int) {
        let content = UNMutableNotificationContent()
        content.title = titleMessage
        content.body = bodyMessage
        content.userInfo = ["emp_id": emp_id]
        content.sound = .default
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: notificationId, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

enum StoryBoard: String {
    case login = "Login"
    case home = "Home"
    case communication = "Communication"
}

enum RingType: String {
    case incoming = "incomingTone"
    case outGoing = "outgoingTone"
    case disconnected = "disconnectTone"
}
