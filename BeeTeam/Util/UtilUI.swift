import UIKit

//MARK:- UITextField
class DropDownTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = ConstantFont.Font_Detail
        self.textColor = ConstantColor.Color_DarkGray
            
        self.addDropDownIcon()
    }
    
    let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

class TextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = ConstantFont.Font_Detail
        self.textColor = ConstantColor.Color_Black
    }
    
//    let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 5)
//
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
}

//MARK:- UIButton
class SolidButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = ConstantColor.Color_Orange
        self.layer.cornerRadius = 8
        self.tintColor = ConstantColor.Color_White
        self.titleLabel?.font = ConstantFont.Font_TitleBold
    }
}

class PlainButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.tintColor = ConstantColor.Color_Orange
        self.titleLabel?.font = ConstantFont.Font_Detail
    }
}

//MARK:- UILabel
class TitleLabel: UILabel {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.textColor = ConstantColor.Color_Black
        self.font = ConstantFont.Font_Detail
    }
}
