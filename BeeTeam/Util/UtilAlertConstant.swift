//
//  UtilAlertConstant.swift
//  BeeTeam
//
//

import Foundation
import UIKit

struct AlertConstant {
    static let loadingMessage = "Loading..."
    static let cancel = "Cancel"
    static let ok = "Ok"
    static let somethingWrong = "Something went wrong. Please try again later"
    static let requestTimeOut = "The request timed out."
    static let connectionLost = "The network connection was lost."
    static let connectionLostMessage = "The network connection was lost.\nPlease try again later."
    static let timeOutMessage = "The request timed out.\nInternet connection seems to be very slow.\nPlease try again later"
    static let noInternetConnection = "No Internet Connection. Please try again."
    static let enterUsername = "Please Enter Username"
    static let enterPassword = "Please enter password"
    static let enterEmail = "Please enter Email"
    static let invalidEmail = "Email is not valid"
}

func alertShowWithOK(_ title : String = "BeeTeam", message : String, viewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: AlertConstant.ok, style: .default, handler: nil))
    viewController.present(alert, animated: true, completion: nil)
}
