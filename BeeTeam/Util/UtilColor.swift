import UIKit

struct ConstantColor {
    static let Color_Yellow = UIColor(red: 248/255, green: 190/255, blue: 21/255, alpha: 1.0)
    static let Color_Orange = UIColor(red: 215/255, green: 109/255, blue: 39/255, alpha: 1.0)
    static let Color_Black = UIColor.black
    static let Color_White = UIColor.white
    
    static let Color_DarkGray = UIColor.darkGray
}
