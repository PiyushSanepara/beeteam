//
//  SingleRClientManager.swift
//  BeeTeam
//
//  Created by Jaydip Gadhiya on 15/08/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import SwiftR
import ObjectMapper

protocol SignalRManagerDelegate: class {
    func newMessage(message: Message?, isSent: Bool)
    func logSentReceipt(msgID: Int?, receivedDateTime: String?)
    func logSentRead(msgID: Int?, readDateTime: String?)
    func callInitiateWith(callId: Int, caller: Employee?, receiver: Employee?)
    func callIncomingChangeStatus(status: Int)
    func callOutgoingChangeStatus(status: Int)
    func callIsNowOngoing(callId: Int, caller: Employee?, receiver: Employee?, roomId: String)
    func callNotFound(callId: Int)
    func onGoingCallNotFound()
}

extension SignalRManagerDelegate {
    func newMessage(message: Message?, isSent: Bool) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func logSentReceipt(msgID: Int?, receivedDateTime: String?) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func logSentRead(msgID: Int?, readDateTime: String?) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func callInitiateWith(callId: Int, caller: Employee?, receiver: Employee?) {
        //this is a empty implementation to allow this method to be optional
    }

    func callIncomingChangeStatus(status: Int) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func callOutgoingChangeStatus(status: Int) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func callIsNowOngoing(callId: Int, caller: Employee?, receiver: Employee?, roomId: String) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func callNotFound(callId: Int) {
        //this is a empty implementation to allow this method to be optional
    }
    
    func onGoingCallNotFound() {
        //this is a empty implementation to allow this method to be optional
    }
}


class SignalRClientManager: NSObject {
    static let shared = SignalRClientManager()
    var connection: SignalR = SignalR("https://ob.navjodev.com")
    let messageHub = Hub("WebRTC")
    weak var delegate: SignalRManagerDelegate?
    
    var isAudioCall: Bool = true
    
    override init() {
        super.init()
    }
    
    func initialEvents(isConnected:@escaping (Bool) -> Void) {
        connection.queryString = ["token": UserDefault.getToken()]
        
//        if isGroupChat {
            addGroupChatCallback()
//        } else {
            addOneToOneChatCallback()
//        }
        receiveCall()
        addCallingEventCallback()
        connection.addHub(messageHub)
        connection.originUrlString = "https://ob.navjodev.com"
        connection.connected = {
            print("connected: \(self.connection.connectionID ?? "")")
            isConnected(true)
        }
        connection.connectionSlow = { print("connectionSlow") }
        connection.reconnecting = { print("reconnecting") }
        connection.reconnected = { print("reconnected") }
        connection.disconnected = {
            let delayTime = DispatchTime.now() + .seconds(4)
            DispatchQueue.main.asyncAfter(deadline: delayTime) { [weak self] in
                self?.connection.start()
            }
        }
        connection.start()
    }
    
    //    Mark:- Personal Message
    func sendMessage(messageData : [Any]) {
        do {
            try messageHub.invoke("pMSndMsg", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                } 
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    func addOneToOneChatCallback() {
        self.getMessage()
        self.getSentMessage()
        self.logSentMessageReceipt()
        self.logSentMessageRead()
    }
    
    func addGroupChatCallback() {
        self.getGroupMessage()
        self.getSentGroupMessage()
        self.logSentGroupMessageReceipt()
        self.logSentGroupMessageRead()
    }
    
    func addCallingEventCallback() {
        empAlreadyInCallWhileCalling()
        empNotFoundWhileCalling()
        empDisabledWhileCalling()
        empReceiverBusyWhileCalling()
        empCallTryingToConnect()
        empCallOutgoingChangeStatus()
        empCallIncomingChangeStatus()
        empCallIsNowOngoingStatus()
        empPingCallOnGoing()
        empCallNotFound()
        ongoingCallNotFound()
    }
    
    func getSentMessage() {
        messageHub.on("receiveSentMessageID") { (_ newMessage) in
            guard newMessage?.count ?? 0 > 1 else { return }
            if let stringData = newMessage?[1] as? String, let message = Mapper<Message>().map(JSONString: stringData) {
                self.delegate?.newMessage(message: message, isSent: true)
            }
        }
    }
    
    func logSentMessageReceipt() {
        messageHub.on("logReceipt") { (_ logReceipt) in
            if logReceipt?.count == 3 {
                self.delegate?.logSentReceipt(msgID: logReceipt?[1] as? Int, receivedDateTime: logReceipt?[2] as? String)
            }
        }
    }
    
    func logSentMessageRead() {
        messageHub.on("logRead") { (_ logRead) in
            if logRead?.count == 3 {
                self.delegate?.logSentRead(msgID: logRead?[1] as? Int, readDateTime: logRead?[2] as? String)
            }
        }
    }
    
    func getMessage() {
        messageHub.on("receiveMessage") { (_ newMessage) in
            guard newMessage?.count ?? 0 > 0 else { return }
            if let stringData = newMessage?[0] as? String, let message = Mapper<Message>().map(JSONString: stringData) {
                self.messageLgRcpt(messageData: [message.id ?? 0])
//                self.messageLgRd(messageData: [message.id ?? 0])
//                CommonMethods.sendLocalNotification(notificationId: NotificationId.receiveMessageLocalNotification, titleMessage: "رسالة جديدة", bodyMessage: "\(message.messageText ?? ""): \(message.senderId ?? 0)", emp_id: message.senderId ?? 0)
                
                CommonMethods.sendLocalNotification(notificationId: NotificationId.receiveMessageLocalNotification, emp_id: message.senderId ?? 0)
                self.delegate?.newMessage(message: message, isSent: false)
            }
        }
    }
    
    func messageLgRcpt(messageData : [Any]) {
        do {
            try messageHub.invoke("pMLgRcpt", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    func messageLgRd(messageData : [Any]) {
        do {
            try messageHub.invoke("pMLgRd", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    //    Mark: Group Message
    func sendGroupMessage(messageData : [Any]) {
        do {
            try messageHub.invoke("gMSndMsg", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    func getSentGroupMessage() {
        messageHub.on("receiveSentGroupMessageID") { (_ newMessage) in
            guard newMessage?.count ?? 0 > 1 else { return }
            if let stringData = newMessage?[1] as? String, let message = Mapper<Message>().map(JSONString: stringData) {
                self.delegate?.newMessage(message: message, isSent: true)
            }
        }
    }
    
    func logSentGroupMessageReceipt() {
        messageHub.on("logGReceipt") { (_ logReceipt) in
            //            print("logGReceipt--------->", logReceipt)
            if logReceipt?.count == 3 {
                self.delegate?.logSentReceipt(msgID: logReceipt?[1] as? Int, receivedDateTime: logReceipt?[2] as? String)
            }
        }
    }
    
    func logSentGroupMessageRead() {
        messageHub.on("logGRead") { (_ logRead) in
            //            print("logGRead--------->", logRead)
            if logRead?.count == 3 {
                self.delegate?.logSentRead(msgID: logRead?[1] as? Int, readDateTime: logRead?[2] as? String)
            }
        }
    }
    
    func getGroupMessage() {
        messageHub.on("receiveGroupMessage") { (_ newMessage) in
            guard newMessage?.count ?? 0 > 0 else { return }
            if let stringData = newMessage?[0] as? String, let message = Mapper<Message>().map(JSONString: stringData) {
                self.groupMessageLgRcpt(messageData: [message.id ?? 0])
//                self.groupMessageLgRd(messageData: [message.id ?? 0])
                self.delegate?.newMessage(message: message, isSent: false)
            }
        }
    }
    
    func groupMessageLgRcpt(messageData : [Any]) {
        do {
            try messageHub.invoke("gMLgRcpt", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    func groupMessageLgRd(messageData : [Any]) {
        do {
            try messageHub.invoke("gMLgRd", arguments: messageData) { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            }
        } catch let error {
            print("----------error--------", error.localizedDescription)
        }
    }
    
    //MARK: Call Method
    
    func receiveCall() {
        messageHub.on("receiveCall") { (_ callDetail) in
            guard callDetail?.count ?? 0 > 0 else { return }
            let data = (callDetail?[0] as? String)?.data(using: .utf8)
            do {
                if let jsonData = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [String: Any]
                {
                    if let callId = jsonData["ID"] as? Int, let caller = jsonData["Caller"] as? [String: Any], let receiver = jsonData["Receiver"] as? [String: Any] {
                        self.navigateToReceiveCallScreen(callId: callId, caller: Employee.init(JSON: caller), receiver: Employee(JSON: receiver))
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func initiateCallWithOption(callOption: [Any]) {
        do {
            try messageHub.invoke("initCall", arguments: callOption, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
        }
    }
    
    func empAlreadyInCallWhileCalling() {
        messageHub.on("alreadyInCall") { (_ newMessage) in
            print("alreadyInCall------->", newMessage)
            guard newMessage?.count ?? 0 > 0 else { return }
        }
    }
    
    func empNotFoundWhileCalling() {
        messageHub.on("empNotFound") { (_ newMessage) in
            print("empNotFound------->", newMessage)
            guard newMessage?.count ?? 0 > 0 else { return }
        }
    }
    
    func empDisabledWhileCalling() {
        messageHub.on("empDisabled") { (_ newMessage) in
            print("empDisabled------->", newMessage)
            guard newMessage?.count ?? 0 > 0 else { return }
        }
    }
    
    func empReceiverBusyWhileCalling() {
        messageHub.on("receiverBusy") { (_ newMessage) in
            print("receiverBusy------->", newMessage)
            guard newMessage?.count ?? 0 > 0 else { return }
        }
    }
    
    func empCallTryingToConnect() {
        messageHub.on("callTryingToConnect") { (_ callDetail) in
            guard callDetail?.count ?? 0 > 0 else { return }
            let data = (callDetail?[0] as? String)?.data(using: .utf8)
            do {
                if let jsonData = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [String: Any]
                {
                    if let callId = jsonData["ID"] as? Int, let caller = jsonData["Caller"] as? [String: Any], let receiver = jsonData["Receiver"] as? [String: Any] {
//                        self.delegate?.callInitiateWith(callId: callId, caller: Employee.init(JSON: caller), receiver: Employee(JSON: receiver))
                        self.navigateToInitiateCallScreen(callId: callId, caller: Employee.init(JSON: caller), receiver: Employee(JSON: receiver))
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func empCallOutgoingChangeStatus() {
        messageHub.on("callOutgoingChangeStatus") { (_ callData) in
            guard callData?.count ?? 0 > 0 else { return }
            self.delegate?.callOutgoingChangeStatus(status: callData?.last as? Int ?? 0)
        }
    }
    
    func empCallIsNowOngoingStatus() {
        messageHub.on("callIsNowOngoing") { (_ roomData) in
            guard roomData?.count ?? 0 > 0 else { return }
            let data = (roomData?[0] as? String)?.data(using: .utf8)
            do {
                if let jsonData = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [String: Any]
                {
                    if let callId = jsonData["ID"] as? Int, let caller = jsonData["Caller"] as? [String: Any], let receiver = jsonData["Receiver"] as? [String: Any], let roomId = roomData?.last as? String  {
                        self.delegate?.callIsNowOngoing(callId: callId, caller: Employee.init(JSON: caller), receiver: Employee.init(JSON: receiver), roomId: roomId)
                    }
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func empCallIncomingChangeStatus() {
        messageHub.on("callIncomingChangeStatus") { (_ callData) in
            guard callData?.count ?? 0 > 0 else { return }
            self.delegate?.callIncomingChangeStatus(status: callData?.last as? Int ?? 0)
        }
    }
    
    func empPingCallOnGoing() {
        messageHub.on("pingOutgoingCall") { (_ callData) in
            guard callData?.count ?? 0 > 0 else { return }
        }
    }
    
    func acceptCall(callOption: [Any]) {
        do {
            try messageHub.invoke("acceptCall", arguments: callOption, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
        }
    }
    
    func endCalls(completion:@escaping (Bool) -> Void) {
        do {
            try messageHub.invoke("endCalls", arguments: nil, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                    completion(false)
                }else {
                    completion(true)
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
            completion(false)
        }
    }
    
    func ongoingCallNotFound() {
        messageHub.on("ongoingCallNotFound") { (_ callData) in
            self.delegate?.onGoingCallNotFound()
        }
    }
    
    func registerCallRinging(callData: [Any]) {
        do {
            try messageHub.invoke("registerCallRinging", arguments: callData, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
        }
    }
    
    func pingCallOnGoing(callData: [Any]) {
        do {
            try messageHub.invoke("pingCallOnGoing", arguments: callData, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
        }
    }
    
    func empCallNotFound() {
        messageHub.on("callNotFound") { (_ callData) in
            guard callData?.count ?? 0 > 0 else { return }
            self.delegate?.callNotFound(callId: callData?.first as? Int ?? 0)
        }
    }
    
    func notificationReadAll(completion:@escaping (Bool) -> Void) {
        do {
            try messageHub.invoke("notificationReadAll", arguments: nil, callback: { (result, error) in
                if let e = error {
                    print("Error: \(e)")
                    completion(false)
                } else {
                    completion(true)
                }
            })
        } catch let error {
            print("error------>", error.localizedDescription)
            completion(false)
        }
    }
}

extension SignalRClientManager {
    func navigateToReceiveCallScreen(callId: Int, caller: Employee?, receiver: Employee?) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let topController = keyWindow?.rootViewController, let viewControllers = (topController as? UINavigationController)?.viewControllers {
            print(topController)
            let vc = viewControllers.first { (vc) -> Bool in
                vc.isKind(of: ReceiverRingViewController.self)
            }
            if vc == nil {
                if let vc = CommonMethods.viewController(name: .communication, identifier: "ReceiverRingViewController") as? ReceiverRingViewController {
                    vc.callId = callId
                    vc.callerData = caller
                    (topController as? UINavigationController)?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func navigateToInitiateCallScreen(callId: Int, caller: Employee?, receiver: Employee?) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let topController = keyWindow?.rootViewController, let viewControllers = (topController as? UINavigationController)?.viewControllers {
            print(topController)
            let vc = viewControllers.first { (vc) -> Bool in
                vc.isKind(of: CallerRingViewController.self)
            }
            if vc == nil {
                if let vc = CommonMethods.viewController(name: .communication, identifier: "CallerRingViewController") as? CallerRingViewController {
                    vc.callId = callId
                    vc.receiverData = receiver
                    vc.isAudioCall = self.isAudioCall
                    (topController as? UINavigationController)?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func navigateToChatScreen(emp_id: Int?) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if let topController = keyWindow?.rootViewController, let viewControllers = (topController as? UINavigationController)?.viewControllers {
            let vc = viewControllers.first { (vc) -> Bool in
                vc.isKind(of: ChatViewController.self)
            }
            if vc == nil {
                if let vc = CommonMethods.viewController(name: .communication, identifier: "TelecomViewController") as? TelecomViewController {
                    vc.notificationSenderId = emp_id
                    
                    let ssSideMenuVC = viewControllers.first { (vc) -> Bool in
                        vc.isKind(of: SSASideMenu.self)
                    }
                    
                    let navVC = ssSideMenuVC?.children.first(where: { (vc) -> Bool in
                        vc.isKind(of: NavigationController.self)
                    })
                    (navVC as? UINavigationController)?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
enum CallStatus: Int {
    case callFailed = 0
    case notReachable = 3
    case favorites = 2
    case notAnswer = 4
    case endedRingingSelf = 5
    case endedRingingOther = 6
    case callOngoing = 7
    case endedCallBySelf = 8
    case endedCallByOther = 9
}
