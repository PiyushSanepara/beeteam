import Foundation
import UIKit
import MBProgressHUD

struct ConstantFormatter {
    static let DateFormat = "dd-MM-yyyy"
    static let DateFormatFullServer = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let DateFormatFull = "yyyy-MM-dd HH:mm:ss"
}

struct UrlConstant {
    static let baseUrl = "https://ob.navjodev.com/api/v1/"
    static let baseUrlImage = "https://ob.navjodev.com"
    static let baseURLMaterialChat = "https://ob.navjodev.com/api/v1/chat/message/material"
    static let baseURLGroupMaterialChat = "https://ob.navjodev.com/api/v1/group/message/material"
    static let baseURLMaterialPost = "https://ob.navjodev.com/api/v1/post/material"
    static let baseURLMaterialStory = "https://ob.navjodev.com/api/v1/story/material"
    static let baseURLVideoThumbnail = "https://ob.navjodev.com/api/v1/Video/Thumbnail"
    static let baseURLEmployeePicture = "https://ob.navjodev.com/api/v1/Employee/Picture"
    static let baseURLGroupPicture = "https://ob.navjodev.com/api/v1/Group/Picture"
}

struct HeaderConstant {
    static let applicationJson = "application/json"
    static let authorization = "Authorization"
}

struct UserDefaultConstant {
    static let token = "token"
    static let userId = "userId"
    static let loginDetails = "loginDetails"
    static let password = "password"
}

struct NotificationId {
    static let receiveMessageLocalNotification = "receiveMessageLocalNotification"
}
