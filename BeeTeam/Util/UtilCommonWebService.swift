//
//  UtilCommonWebService.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 25/07/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import Foundation
import Alamofire

class UtilCommonWebService {
    static func socialUpload(qqfile: Data?, qqfilename: String?, successHandler : @escaping(_ objects: Int?) -> Void, errorHandler : @escaping(_ error: String) -> Void ) {
            
            let headers: HTTPHeaders = [
                HeaderConstant.authorization : UserDefault.getToken()
            ]
            
            let socialUploadURL = "\(UrlConstant.baseUrl)" + "Social/Upload"
                    
            var params = [String: Any]()
            
            if qqfile != nil {
                params["qquuid"] = UIDevice.current.identifierForVendor?.uuidString
                params["qqfilename"] = qqfilename ?? "demoImageName"
            }
            
            showProgressHud(progressText: "Upload")

            
            Alamofire.upload(multipartFormData: { multipartFormData in
                for (key, value) in params {
                    if let value = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                        multipartFormData.append(value, withName: key)
                    }
                }
                if let imgData = qqfile {
                    multipartFormData.append(imgData, withName: qqfilename ?? "", fileName: qqfilename ?? "", mimeType: "*")//image/jpg
                }
            }, usingThreshold: UInt64.init(), to: socialUploadURL, method: .post, headers: headers) { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (_) in })
                    upload.responseJSON { response in
                        hideProgressHud()
                        if let dict = response.result.value as? NSDictionary {
                            print(dict)
                            successHandler(dict["id"] as? Int)
    //                        completion(dict, nil)
                        }
                    }
                case .failure(let error):
    //                    print(error)
                    hideProgressHud()
                    errorHandler(error.localizedDescription)
    //                completion(nil, encodingError)
                }
            }
        }
}
