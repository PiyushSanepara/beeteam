import UIKit

struct ConstantFont {
    static let Font_TitleBold = UIFont.boldSystemFont(ofSize: 21.0)
    static let Font_Title = UIFont.systemFont(ofSize: 21.0)
    static let Font_Detail = UIFont.systemFont(ofSize: 17.0)
}
