//
//  HttpRouter.swift
//  BeeTeam
//

import Foundation
import Alamofire
import OneSignal

enum HttpRouter: URLRequestConvertible {
    
    case loginUser(empId: String, empPass:String)
    case forgotPassword(email: String)
    case pepole(empId: Int)
    case posts(next: String?, person: Int?, department: Int?)
    case notifications
    case unreadCount
    case likes(next: String?, id: Int?)
    case comments(next: String?, id: Int?)
    case stories(next: String?)
    case toggleLike(id: Int, like: Bool)
    case createComment(id: Int, In_reply_to: Int?, CommentString: String?)
    case getStories(id: Int)
    case myStories(next: String?)
    case createStory(materialID: String?, text_Content: String?)
    case createPost(materialID: String?, text_Content: String?, postId: Int?)
    case updateProfileImage(materialID: String?)
    case chatList(count: Int)
    case groupChatList(count: Int)
    case chat(count: Int, secondParty: Int, beforeTime: String?)
    case groupChat(count: Int, groupId: Int, beforeTime: String?)
    case callHistory(next: String?)
    case peopleSearch(next: String?)
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .loginUser,.forgotPassword, .toggleLike, .createComment, .createStory, .createPost, .updateProfileImage:
            return .post
        case .pepole,.posts, .notifications, .unreadCount, .likes, .comments, .stories, .getStories, .myStories, .chatList, .groupChatList, .chat, .groupChat, .callHistory, .peopleSearch:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .loginUser:
            return "Login/iOS"
        case .forgotPassword:
            return "ForgotPassword"
        case .pepole:
            return "People"
        case .posts(let next, _, _):
            return "Social/Posts?\(next ?? "After=0&Quantity=15")"
        case .notifications:
            return "User/Notifications"
        case .unreadCount:
            return "User/Notifications/UnreadCount"
        case .likes(let next, _):
            return "Social/Post/Likes?\(next ?? "after=0&quantity=15")"
        case .comments(let next, _):
            return "Social/Post/comments?\(next ?? "after=0&quantity=15")"
        case .stories(let next):
            return "Social/Stories?\(next ?? "After=0&Quantity=15")"
        case .toggleLike:
            return "Social/Post/ToggleLike"
        case .createComment:
            return "Social/Post/comment/create"
        case .getStories:
            return "Social/Employee/Stories"
        case .myStories(let next):
            return "User/Social/Stories?\(next ?? "After=0&Quantity=15")"
        case .createStory:
            return "Social/Story/create"
        case .createPost:
            return "Social/Post/create"
        case .updateProfileImage:
            return "User/pic/edit?id=\(UserDefault.getLoginDetails()?.employee?.id ?? 0)"
        case .chatList:
            return "User/Contacts"
        case .groupChatList:
            return "User/Groups"
        case .chat:
            return "User/Messages"
        case .groupChat:
            return "Group/Messages"
        case .callHistory:
            return "Calls/History"
        case.peopleSearch(let next):
            return "People/Search?\(next ?? "skip=0&name=")"
        }
    }

    var jsonParameters: [String: Any]? {
        switch self {
        case .loginUser(let empId, let empPass):
            return ["EMP_ID": empId, "EMP_PASS": empPass, "Notification_Device_ID": OneSignal.getPermissionSubscriptionState()?.subscriptionStatus.userId ?? NSNull.self]
        case .forgotPassword(let email):
            return ["Email": email]
        case .toggleLike(let id, let like):
            return ["id": id, "Like": like]
        case .createStory(let materialID, let text_Content):
            return ["Material": materialID ?? "",
                    "Text_Content": text_Content ?? "",
                    "Background_Color": "ffffff",
                    "Target_Audience": "1"]
        case .createPost(let materialID, let text_Content, let postId):
            let isDepartment = !((UserDefault.getLoginDetails()?.employee?.userPermissions?.smPost ?? 0) > 2)
            var params = [String: Any]()
            
            if let id = postId {
                params["Base_Post"] = id
            }
            if let mId = materialID {
                params["Materials"] = mId
            }
            params["Text_Content"] = text_Content ?? ""
            params["As_Department"] = isDepartment
            params["Target_Audience"] = "1"
            
            return params
        case .updateProfileImage(let materialID):
            return ["ID" : materialID ?? 0, "En_Name": UserDefault.getLoginDetails()?.employee?.enFullName ?? "", "Ar_Name": UserDefault.getLoginDetails()?.employee?.arFullName ?? "", "Status": true]
        default:
            return nil
        }
    }
    
    var headers: Bool? {
        switch self {
        default:
            return true
        }
    }
    
    var urlParameters: [String: Any]? {
        switch self {
        case .pepole(let empId):
            return ["id": empId]
        case .createComment(let id, let In_reply_to, let CommentString):
            var params = [String: Any]()
            params["id"] = id
            if let reply_to = In_reply_to {
                params["In_reply_to"] = reply_to
            }
            if let comment = CommentString {
                params["CommentString"] = comment
            }
            return params
        case .getStories(id: let empId):
            return ["id": empId]
        case .posts(_, let person, let department):
            var reqArr = [String: Any]()
            if let p = person {
                reqArr["person"] = p
            }
            if let d = department {
                reqArr["department"] = d
            }
            return reqArr
            
        case .notifications:
            return ["skip": 0]//change with pagination
            
        case .likes(_, let id):
            return ["id": id as Any]
            
        case .comments(_, let id):
            return ["id": id as Any]
            
        case .chatList(let count):
            return ["count": count]
            
        case .groupChatList(let count):
            return ["count": count]
            
        case .chat(let count, let secondParty, let beforeTime):
            return ["SecondParty": secondParty,
                    "BeforeTime": beforeTime ?? "",
                    "count": count]
            
        case .groupChat(let count, let groupId, let beforeTime):
            return ["groupId": groupId,
                    "BeforeTime": beforeTime ?? "",
                    "count": count]
            
        default:
            return nil
        }
    }
    
    var isAuthToken: Bool {
        switch self {
        case .pepole, .posts, .notifications, .unreadCount, .likes, .comments, .stories, .toggleLike, .createComment, .getStories ,.myStories, .createStory, .createPost, .updateProfileImage, .chatList, .groupChatList , .chat, .groupChat, .callHistory, .peopleSearch:
            return true
        default:
            return false
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = NSURL(string: "\(UrlConstant.baseUrl)" + "\(self.path.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) ?? "")")!
        var urlRequest = URLRequest(url: url as URL)
        urlRequest.timeoutInterval = 30
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue(HeaderConstant.applicationJson, forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(HeaderConstant.applicationJson, forHTTPHeaderField: "accept")
        
        if self.isAuthToken {
            urlRequest.setValue(UserDefault.getToken(), forHTTPHeaderField: HeaderConstant.authorization)
        }
        
        switch self {
        case .loginUser, .forgotPassword, .toggleLike, .createStory, .createPost, .updateProfileImage:
            do {
                let jsonData: NSData = try JSONSerialization.data(withJSONObject: self.jsonParameters ?? Dictionary(), options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
                print("JSON Request: \(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)")
            }
            catch let error as NSError {
                print("Could not prepare request: \(error), \(error.userInfo)")
            }
            return try JSONEncoding.default.encode(urlRequest, with: self.jsonParameters)
      //MARK: For get methods API
        case .pepole, .posts, .notifications, .unreadCount, .likes, .comments, .stories, .createComment, .getStories, .myStories, .chatList, .groupChatList, .chat, .groupChat, .callHistory, .peopleSearch:
             return try URLEncoding.queryString.encode(urlRequest, with: self.urlParameters)
        }
    }
}
