//
//  NetworkManager.swift
//  BeeTeam
//

import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import BrightFutures
import MBProgressHUD

enum NetworkError: Error {
    case notFound
    case unauthorized(String?)
    case unauthorizedUser(String?)
    case forbidden
    case nonRecoverable(String?)
    case unprocessableEntity(String?)
    case noInternet(String)
    case requestTimeOut(String)
    case customErrorMessage(String)
    case other
}

struct NetworkManager {
    
    static let networkQueue = DispatchQueue(label: "\(String(describing: Bundle.main.bundleIdentifier)).networking-queue", attributes: .concurrent)
    
    static func makeRequest<T: Mappable>(_ urlRequest: URLRequestConvertible,
                                         isShowProgress: Bool = false,
                                         ProgrssTitle:String = AlertConstant.loadingMessage) -> Future<T, NetworkError> {
        
        let promise = Promise<T, NetworkError>()
                
        if isShowProgress {
            showProgressHud(progressText: ProgrssTitle)
        }
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.configuration.timeoutIntervalForRequest = 180
        let request = sessionManager.request(urlRequest)
            .validate()
            .responseObject(queue: networkQueue) { (response: DataResponse<T>)-> Void in
                #if FeliTest
                print("\nResponse: \(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)!)\n")
                #endif
                URLCache.shared.removeAllCachedResponses()
                if isShowProgress {
                    hideProgressHud()
                }
                switch response.result {
                case .success:
                    promise.success(response.result.value!)
                case .failure
                    where response.response?.statusCode == 401 || response.response?.statusCode == 403 :
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.unauthorized(jsonData))
                case .failure
                    where response.response?.statusCode == 403 :
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.unauthorizedUser(jsonData))
                case .failure
                    where response.response?.statusCode == 404:
                    promise.failure(.notFound)
                case .failure
                    where (response.response?.statusCode == 422 || response.response?.statusCode == 400) :
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.unprocessableEntity(jsonData))
                case .failure
                    where response.response?.statusCode == 500:
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.nonRecoverable(jsonData))
                case .failure
                    where response.response?.statusCode == nil && response.result.error?.localizedDescription == AlertConstant.requestTimeOut:
                    promise.failure(.requestTimeOut(AlertConstant.timeOutMessage))
                case .failure
                    where response.response?.statusCode == nil && !Connectivity.isConnectedToInternet:
                    promise.failure(.requestTimeOut(AlertConstant.connectionLostMessage))
                case .failure:
                    promise.failure(.other)
                }
        }
//        #if FeliTest
        print("\nRequest: ")
        debugPrint(request)
        print("\n")
//        #endif
        return promise.future
    }
    
    static func makeJSONObjectArrayRequest(_ urlRequest: URLRequestConvertible,
                                           isPrintLog: Bool = true,
                                           isShowProgress: Bool = false,
                                           ProgrssTitle:String = AlertConstant.loadingMessage) -> Future<[Dictionary<String, Any>], NetworkError> {
        
        let promise = Promise<[Dictionary<String, Any>], NetworkError>()
        if isShowProgress {
            showProgressHud(progressText: ProgrssTitle)
        }
        
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.configuration.timeoutIntervalForRequest = 180
        let request = sessionManager.request(urlRequest)
            .validate()
            .responseJSON(queue: networkQueue) { response in
                #if FeliTest
                print(response.result)
                print("\nResponse: \(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)!)\n")
                #endif
                URLCache.shared.removeAllCachedResponses()
                if isShowProgress {
                    hideProgressHud()
                }
                switch response.result {
                case .success(let JSON):
                    if let jsonObject = JSON as? [Dictionary<String, Any>] {
                        promise.success(jsonObject)
                    }
                    else if let jsonObject = JSON as? Dictionary<String, Any> {
                        promise.success([jsonObject])
                    }
                    else {
                        promise.failure(.other)
                    }
                case .failure
                    where response.response?.statusCode == 401:
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.unauthorized(jsonData))
                case .failure
                    where response.response?.statusCode == 403:
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.unauthorizedUser(jsonData))
                case .failure
                    where response.response?.statusCode == 404:
                    promise.failure(.notFound)
                case .failure
                    where response.response?.statusCode == 500:
                    var jsonData: String?
                    if let data = response.data {
                        jsonData = String(data: data, encoding: .utf8)
                    }
                    promise.failure(.nonRecoverable(jsonData))
                case .failure
                    where response.response?.statusCode == nil && response.result.error?.localizedDescription == AlertConstant.requestTimeOut:
                    promise.failure(.requestTimeOut(AlertConstant.timeOutMessage))
                case .failure
                    where response.response?.statusCode == nil && response.result.error?.localizedDescription == AlertConstant.connectionLost:
                    promise.failure(.requestTimeOut(AlertConstant.connectionLostMessage))
                case .failure:
                    promise.failure(.other)
                }
        }
        if isPrintLog {
            debugPrint(request)
        }
        return promise.future
    }
    
    static func makeRequest<T: Mappable>(_ uploadRequest: UploadRequest, isShowProgress: Bool = false, ProgrssTitle:String = "Loading...") -> Future<T, NetworkError> {
            let promise = Promise<T, NetworkError>()
            
            if isShowProgress {
                showProgressHud(progressText: ProgrssTitle)
            }
            
            uploadRequest.validate()
                .responseObject(queue: networkQueue) { (response: DataResponse<T>)-> Void in
                    if let data = response.data {
                        print("\nResponse: \(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)\n")
                    }
                    if isShowProgress {
                        hideProgressHud()
                    }
                    switch response.result {
                    case .success:
                        promise.success(response.result.value!)
                    case .failure
                        where response.response?.statusCode == 401 || response.response?.statusCode == 403 :
                        var jsonData: String?
                        if let data = response.data {
                            jsonData = String(data: data, encoding: .utf8)
                        }
                        promise.failure(.unauthorized(jsonData))
                    case .failure
                        where response.response?.statusCode == 403 :
                        var jsonData: String?
                        if let data = response.data {
                            jsonData = String(data: data, encoding: .utf8)
                        }
                        promise.failure(.unauthorizedUser(jsonData))
                    case .failure
                        where response.response?.statusCode == 404:
                        promise.failure(.notFound)
                    case .failure
                        where (response.response?.statusCode == 422 || response.response?.statusCode == 400) :
                        var jsonData: String?
                        if let data = response.data {
                            jsonData = String(data: data, encoding: .utf8)
                        }
                        promise.failure(.unprocessableEntity(jsonData))
                    case .failure
                        where response.response?.statusCode == 500:
                        var jsonData: String?
                        if let data = response.data {
                            jsonData = String(data: data, encoding: .utf8)
                        }
                        promise.failure(.nonRecoverable(jsonData))
                    case .failure:
                        promise.failure(.other)
                    }
            }
    //        print("\nRequest: ")
    //        debugPrint(request)
            print("\n")
            return promise.future
        }

}
