//
//  AppDelegate.swift
//  BeeTeam
//
//  Created by Piyush Sanepara on 31/05/20.
//  Copyright © 2020 Piyush Sanepara. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import OneSignal
import JitsiMeet
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledToolbarClasses = [HomeViewController.self, ChatViewController.self, ProfileViewController.self]
        OneSignal.setLogLevel(.LL_NONE, visualLevel: .LL_NONE)
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
        OneSignal.initWithLaunchOptions(launchOptions, appId: "e3d91eb0-0c38-4bef-8f1f-b40cf51013bf", handleNotificationAction: nil, settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "back-icon")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back-icon")
        let textAttributes = [NSAttributedString.Key.foregroundColor:ConstantColor.Color_Yellow, NSAttributedString.Key.font: ConstantFont.Font_TitleBold]
        UINavigationBar.appearance().titleTextAttributes = textAttributes
        
        
        UNUserNotificationCenter.current().delegate = self
        
        return true
    }
    

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return JitsiMeet.sharedInstance().application(application, continue: userActivity, restorationHandler: restorationHandler)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return JitsiMeet.sharedInstance().application(app, open: url, options: options)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.identifier == NotificationId.receiveMessageLocalNotification {//LocalNotification
            if isChatVCPresent() {
                completionHandler([])
            }
        }
        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.identifier == NotificationId.receiveMessageLocalNotification {
            guard let emp_id = response.notification.request.content.userInfo["emp_id"] as? Int else {
                return
            }
            SignalRClientManager.shared.navigateToChatScreen(emp_id: emp_id)
        }else {
            if let customData = response.notification.request.content.userInfo["custom"] as? [String: Any], let notificationObject = customData["a"] as? [String: Any] {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    SignalRClientManager.shared.navigateToChatScreen(emp_id: notificationObject["senderId"] as? Int)
                }
            }
        }
    }
    
    func isChatVCPresent() -> Bool {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if let topController = keyWindow?.rootViewController, let viewControllers = (topController as? UINavigationController)?.viewControllers {
            let ssSideMenuVC = viewControllers.first { (vc) -> Bool in
                vc.isKind(of: SSASideMenu.self)
            }
            
            let navVC = ssSideMenuVC?.children.first(where: { (vc) -> Bool in
                vc.isKind(of: NavigationController.self)
            }) as? NavigationController
            
            let vc = navVC?.viewControllers.last
            return vc?.isKind(of: ChatViewController.self) ?? false
        }
        return false
    }
}
